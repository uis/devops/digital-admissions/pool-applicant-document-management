"""
Utility functions for handling URLs.

"""
import urllib.parse


def merge_url_query(url, query=None):
    """
    Merge a query dict into an existing URL.

    When given a plain URL and no query, the original URL is returned.

    >>> merge_url_query('https://example.com/')
    'https://example.com/'
    >>> merge_url_query('https://example.com/?foo=1')
    'https://example.com/?foo=1'

    New query parameters are appended to the URL and do not replace existing query parameters.

    >>> merge_url_query('https://example.com/', {'foo': 'bar', 'buzz': 'quux'})
    'https://example.com/?foo=bar&buzz=quux'
    >>> merge_url_query('https://example.com/?foo=one', {'foo': 'two', 'buzz': 'quux'})
    'https://example.com/?foo=one&foo=two&buzz=quux'

    """
    query = query if query is not None else {}

    # Parse the existing url into parts
    url_parts = urllib.parse.urlparse(url)

    # Parse any existing query into a list of name value pairs
    query_list = urllib.parse.parse_qsl(url_parts.query)

    # Append any values from the new query.
    query_list.extend(query.items())

    # Re-assemble the URL substituting the new query string
    new_query = urllib.parse.urlencode(query_list)
    new_url = urllib.parse.urlunparse((
        part if idx != 4 else new_query
        for idx, part in enumerate(url_parts)
    ))

    return new_url
