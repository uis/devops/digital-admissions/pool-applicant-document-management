import logging

import gspread
from gspread.utils import rowcol_to_a1

from ugraddrivesync import fetch_applications

from . import get_credentials, get, get_required_setting

LOG = logging.getLogger(__name__)

# Google Drive API scopes
SCOPES = [
    'https://spreadsheets.google.com/feeds',
    'https://www.googleapis.com/auth/drive',
]

POOLTYPE_FIELD_NAME = 'poolTypeDescription'

UCAS_ID_FIELD_NAME = 'candidate.ucasPersonalId'

# The field names in the SMI records that map to a EoIML spreadsheet row.
ROW_MAPPINGS = (
    'subjectDescription', UCAS_ID_FIELD_NAME, 'candidate.lastName', 'candidate.forenames',
    POOLTYPE_FIELD_NAME, 'collegePreference', 'admitYear',
)

# The index of the pool type (in a list representing a row).
POOLTYPE_INDEX = ROW_MAPPINGS.index(POOLTYPE_FIELD_NAME)

# The index of the UCAS id (in a list representing a row).
UCAS_ID_INDEX = ROW_MAPPINGS.index(UCAS_ID_FIELD_NAME)

# The value the pool type field is set to that indicate a row can no longer matched with a fetched
# application.
POOLTYPE_WITHDRAWN = 'Withdrawn'

# Set of college codes
COLLEGE_CODES = {
    'CAI', 'CC', 'CHR', 'CHU', 'CL', 'CTH', 'DOW', 'ED',
    'EM', 'F', 'G', 'HH', 'HO', 'JE', 'JN', 'K',
    'LC', 'M', 'N', 'MUR', 'PEM', 'PET', 'Q', 'R',
    'SE', 'SID', 'T', 'TH', 'W',
}


def create_row_values(application):
    """
    Creates a list spreadsheet row values from the SMI application
    """
    return [str(get(application, mapping)) for mapping in ROW_MAPPINGS]


def update(settings, *, dry_run=False):
    """
    Synchronises a list of applications queried from SMI with the "Expression of Interest" Google
    spreadsheet. This spreadsheet consists of a "CAO Master List" worksheet with all of the pool
    applications and a worksheet for each of the colleges (named with the college id).
    The first 7 columns are sync targets and
    other columns are ignored. Rows are matched using the application id and are add and amended as
    appropriate but not deleted.

    """
    # Configure applicant source

    source_url = get_required_setting(settings, 'eoiml.source.url')
    source_queries = get(settings, 'eoiml.source.queries', [])
    source_headers = get(settings, 'eoiml.source.headers', {})

    # Fetch all the applications we need to sync. Add each application to a list of all
    # applications and to a list for the particular college.

    all_applications = []
    applications_by_college = {}

    LOG.info('Fetching all applications')
    for query in source_queries:
        for application in fetch_applications(url=source_url, query=query, headers=source_headers):
            all_applications.append(application)
            applications = applications_by_college.get(application['collegePreference'], [])
            applications.append(application)
            applications_by_college[application['collegePreference']] = applications

    LOG.info('Fetched %s applications', len(all_applications))

    # Open the spreadsheet

    sheet_key = get_required_setting(settings, 'eoiml.sheet.key')
    master_worksheet = get_required_setting(settings, 'eoiml.sheet.master_worksheet')

    credentials = get_credentials(settings, scopes=SCOPES)
    gc = gspread.authorize(credentials)
    spreadsheet = gc.open_by_key(sheet_key)

    # Synchronise each of the worksheet with the appropriate list of applications

    master_worksheet_header_rows = get(settings, 'eoiml.sheet.master_worksheet_header_rows', 3)
    college_worksheet_header_rows = get(settings, 'eoiml.sheet.college_worksheet_header_rows', 2)

    for worksheet in spreadsheet.worksheets():
        if worksheet.title == master_worksheet:
            # synchronise the master list
            sync_worksheet(dry_run, all_applications, worksheet, master_worksheet_header_rows)
        elif worksheet.title in COLLEGE_CODES:
            # synchronise the college list
            sync_worksheet(dry_run, all_applications, worksheet, college_worksheet_header_rows)
        else:
            # this isn't a worksheet we are interested in
            LOG.info('%s: ignoring', worksheet.title)

    if len(applications_by_college):
        LOG.warn(
            'There were applications but no worksheet for the following colleges: %s',
            ','.join(applications_by_college.keys())
        )


def sync_worksheet(dry_run, applications, worksheet, header_rows):
    """
    Synchronises a list of applications with a worksheet. This could either be a master sheet
    or a college specific sheet.
    """
    # create a map of the applicant rows keyed on application id
    all_rows = worksheet.range(header_rows + 1, 1, worksheet.row_count, len(ROW_MAPPINGS))
    rows_by_id = {
        row[UCAS_ID_INDEX].value: row
        for row in chunks(all_rows, len(ROW_MAPPINGS)) if row[UCAS_ID_INDEX].value
    }

    # get a count of the rows currently populated
    current_row_count = len(rows_by_id)

    # Changes to existing rows. A list of range updates of the form
    # {'range': a1_range, 'values': [[], [], ..]} for submission to
    # gspread.models.Worksheet.batch_update().
    updates = []
    # A list of lists of strings representing new spreadsheet rows.
    new_rows = []

    for application in applications:
        # match application to row and decide what to do with it
        row = rows_by_id.pop(get(application, UCAS_ID_FIELD_NAME), None)
        new_values = create_row_values(application)
        if row is None:
            new_rows.append(new_values)
        else:
            updates.extend(diff(row, new_values))

    # Handle application is present in the spreadsheet but not in the SMI query results. This
    # could happen if an application is moved out of a pool.
    for application_id, row in rows_by_id.items():
        if row[POOLTYPE_INDEX].value != POOLTYPE_WITHDRAWN:
            # We denote deletion by setting the pooltype to a special value of 'Withdrawn'
            updates.append(
                create_update(rowcol_to_a1(row[0].row, POOLTYPE_INDEX + 1), [[POOLTYPE_WITHDRAWN]])
            )

    LOG.info('%s: %s applicants to add', worksheet.title, len(new_rows))
    LOG.info('%s: %s changes to make to applicant rows', worksheet.title, len(updates))

    # if this is a dry run then finsh here
    if dry_run:
        LOG.info('dry run mode - no changes made')
        return

    # see if there any empty rows at the end of the sheet
    empty_rows = worksheet.row_count - current_row_count - header_rows

    # fill these empty rows with as many applications as will fit
    new_rows_using_batch_update = new_rows[:empty_rows]
    if new_rows_using_batch_update:
        start_row = header_rows + current_row_count + 1
        end_row = start_row + len(new_rows_using_batch_update) - 1
        updates.append({
            'range': a1_range(start_row, 1, end_row, len(ROW_MAPPINGS)),
            'values': new_rows_using_batch_update
        })

    # update the spread with changes to existing applications and new applications in existing
    # empty rows
    worksheet.batch_update(updates, value_input_option='RAW')

    # append any remaining new applications to the sheet
    new_rows_using_append_rows = new_rows[empty_rows:]
    if new_rows_using_append_rows:
        worksheet.append_rows(new_rows_using_append_rows, value_input_option='RAW')


def diff(row, values):
    """
    Diffs an existing row of sheet cell with a list of values. Any diffs are return as an update
    instruction (a range and value) that can be submitted to
    gspread.models.Worksheet.batch_update().
    """
    updates = []
    for cell, value in zip(row, values):
        if cell.value != value:
            updates.append(create_update(rowcol_to_a1(cell.row, cell.col), [[value]]))

    return updates


def create_update(a1_range, values):
    """
    Create a cell range value update that can be submitted to
    gspread.models.Worksheet.batch_update().
    """
    return {'range': a1_range, 'values': values}


def a1_range(from_row, from_col, to_row, to_col):
    """Utility to convert a 4 coordinate range to an A1 range."""
    return f'{rowcol_to_a1(from_row, from_col)}:{rowcol_to_a1(to_row, to_col)}'


def chunks(list, n):
    """Yield successive n-sized chunks from lst."""
    for i in range(0, len(list), n):
        yield list[i:i + n]
