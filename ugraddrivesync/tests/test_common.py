import pytest
from requests.exceptions import HTTPError
from time import time
from ugraddrivesync import ProcessTimeout, api_request
from unittest.mock import Mock

RETRYABLE_STATUS_CODE = 429


def assert_takes(start_time, duration, error_margin=0.02):
    """Assert that `duration` seconds have occurred since `start_time` within `error_margin`"""
    assert start_time + duration < time() + error_margin
    assert start_time + duration > time() - error_margin


def test_api_request_success():
    """Checked that a successful request returns immediately"""
    request_fn = Mock()
    start_time = time()
    api_request(request_fn, lambda: False, None, None)
    assert_takes(start_time, 0)
    request_fn.assert_called_once()


def test_api_request_already_timed_out():
    """Check that a request isn't executed if the timeout has already been exceeded"""
    request_fn = Mock()
    with pytest.raises(ProcessTimeout):
        api_request(request_fn, lambda: True, None, None)
    request_fn.assert_not_called()


def test_api_request_error_no_retry():
    """Check that a non-request error is raised immediately"""
    request_fn = Mock(side_effect=KeyError())
    with pytest.raises(KeyError):
        api_request(request_fn, lambda: False, HTTPError, None)


def test_api_request_http_error_no_retry():
    """Check that a request error with a non retryable status code is raised immediately"""
    request_fn = Mock(side_effect=HTTPError())
    with pytest.raises(HTTPError):
        api_request(request_fn, lambda: False, HTTPError, lambda _: 404)


def test_api_request_retries_then_succeeds():
    """
    Check the elapsed time for a request with a retryable status code error that is retried and
    then succeeds
    """
    called = False

    def request_fn():
        nonlocal called
        if not called:
            called = True
            raise HTTPError

    start_time = time()
    api_request(request_fn, lambda: False, HTTPError, lambda _: RETRYABLE_STATUS_CODE)
    assert_takes(start_time, 5)


def test_api_request_retries_then_fails():
    """
    Check the elapsed time (and retry count) for a request with a retryable status code error that
    is retried and then succeeds
    """
    retries = 2
    request_fn = Mock(side_effect=HTTPError())
    start_time = time()
    with pytest.raises(HTTPError):
        api_request(request_fn, lambda: False, HTTPError, lambda _: RETRYABLE_STATUS_CODE, retries)
    assert_takes(start_time, 15, 0.5)
    assert request_fn.call_count == retries + 1


def test_api_request_timeout_during_retry():
    """Check that a request can timeout during a retry"""
    timeout = 10
    request_fn = Mock(side_effect=HTTPError())
    start_time = time()
    with pytest.raises(ProcessTimeout):
        api_request(
            request_fn, lambda: time() - start_time > timeout, HTTPError,
            lambda _: RETRYABLE_STATUS_CODE
        )
    assert_takes(start_time, timeout, 0.1)
