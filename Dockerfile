FROM registry.gitlab.developers.cam.ac.uk/uis/devops/infra/dockerimages/python:3.8-slim
LABEL maintainer="University of Cambridge Information Services <devops@uis.cam.ac.uk>"

RUN apt-get -y update && apt-get -y install git wkhtmltopdf

WORKDIR /usr/src/app

# Pre-install requirements to make re-build times smaller when developing
ADD ./requirements /usr/src/app/requirements
RUN \
	pip install --upgrade pip && \
	pip install -r /usr/src/app/requirements/requirements.txt && \
	pip install geddit@git+https://gitlab.developers.cam.ac.uk/uis/devops/lib/geddit.git && \
	pip install -r /usr/src/app/requirements/requirements-docker.txt

# Install the tool itself - we don't delete the added files in order that tox
# can continue to be run.
ADD ./ /usr/src/app/
RUN pip install /usr/src/app

# Set up webapp entrypoint
ENV PORT=8000
ENTRYPOINT ["./entrypoint.sh"]
