import collections
import logging
import operator
import sys
from functools import reduce
from xml.etree import ElementTree
from urllib.parse import urljoin

import requests
from requests.auth import HTTPBasicAuth

from ugraddrivesync import fetch_applications, timeout_checker

LOG = logging.getLogger(__name__)

# Defines how CamSIS statuses map to their value in SMI.
STATUS_MAPPINGS = {
    'AC': 'ADMITTED', 'AD': 'OFFER', 'AP': 'APPLICANT', 'CN': 'CANCELLED', 'WT': 'WAITLISTED'
}

# Defines how CamSIS pool types map to their value in SMI.
POOL_TYPE_MAPPINGS = {None: 'NONE', 'S': 'SUMMER', 'W': 'WINTER', 'A': 'ADJUSTMENT'}

# Defines the name of the SMI pool type field.
POOL_TYPE_FIELD = 'poolType'

# Defines how CamSIS pool options map to their value in SMI.
# Note: The SMI doesn't store the information about the pool type in the pool status field
POOL_STATUS_MAPPINGS = {
    None: 'NONE',
    'SPNT': 'NONE',  # Summer Pool No Tag, there is no tagging in the summer pool so this is NONE
    'ADJU': 'NONE',  # UCAS Adjustment, this is a pool type not a status
    'WPIN': 'INTERVIEW',  # Winter Pool Interview
    'WPNT': 'NOTAG',  # Winter Pool No Tag
    'WPTG': 'TAG',  # Winter Pool with Tag
    'WPDT': 'DOUBLETAG',  # Winter Pool Double Tag
    'WPAS': 'REASSESSMENT',  # Winter Pool Reassessment
}

# Defines how to convert fields in a CamSIS applicant record to an SMI application record. The key
# is the CamSIS field and the value is a tuple of the SMI field and a function to convert the
# value. A converter is always required, so if no conversion is necessary, supply an identity.
FIELD_MAPPINGS = {
    'applicationNumber': ('id', lambda v: v),
    # CamSIS still uses `NH` to identify "Murray Edwards College" so it is converted to `MUR` by
    # this converter.
    'college': ('collegePreference', lambda v: 'MUR' if v == 'NH' else v),
    'academicPlan': ('subject', lambda v: v),
    'admissionYear': ('admitYear', lambda v: int(v)),
    'applicationYear': ('year', lambda v: int(v)),
    'status': ('camsisStatus', lambda v: STATUS_MAPPINGS[v]),
    'poolType': (POOL_TYPE_FIELD, lambda v: POOL_TYPE_MAPPINGS[v]),
    'poolOption': ('poolStatus', lambda v: POOL_STATUS_MAPPINGS[v]),
    'ucasPersonalid': ('candidate.ucasPersonalId', lambda v: v),
    'usn': ('candidate.camsisUsn', lambda v: v),
    'lastName': ('candidate.lastName', lambda v: v),
    # The following fields are only used in calculated fields
    'firstName': ('tempFirstName', lambda v: v),
    'middleName': ('tempMiddleName', lambda v: v),
}

# Defines how to calculated fields are populated after converting the CamSIS XML to an object
CALCULATED_FIELD_MAPPINGS = {
    # Join the first and last names with a space. The filter makes sure that if either of the names
    # are blank or missing then we don't add an extra space
    'candidate.forenames': lambda camsis_app: ' '.join(filter(None, [
        camsis_app.get("tempFirstName"),
        camsis_app.get("tempMiddleName"),
    ]))
}

# A list of default values to use when inserting applicants into the SMI from CamSIS data
# This only includes fields that are known to not be provided by CamSIS
INSERT_DEFAULT_FIELD_VALUES = {
    'subjectOptions': []
}

# A list of which pools are being synchronised by the process.
TARGET_POOLS = ('WINTER', 'SUMMER', 'ADJUSTMENT')

# A list of which fields that should always be synchronised
ALWAYS_SYNC = (
    'camsisStatus',
    POOL_TYPE_FIELD,
    'poolStatus',
    'subject',
    'admitYear',
    'candidate.forenames',
    'candidate.lastName',
)

# A list of fields that should be synchronised if the applicant isn't in a target pool.
SYNC_UNLESS_IN_TARGET_POOLS = ('collegePreference', )

# A list of fields that should never be sync'd or created
NEVER_CREATE_OR_SYNC = ('tempFirstName', 'tempMiddleName',)

# Default minimum number of applicants expected to be returned by the CamSIS feed
DEFAULT_MIN_EXPECTED_APPLICATION_COUNT = 1000

# Modes in which the import_camsis_data function can be run
# insert - Inserts missing applicants into the SMI
# update - Updates existing applicant data in the SMI
# upsert - Both insert and update applicant data
# delete - Delete applicants from the SMI if they don't appear in CamSIS
# sync - Synchronise the SMI applicant records with CamSIS feed. Inserting, updating,
#        or deleting as apppriate
CAMSIS_IMPORT_MODES = ('insert', 'update', 'upsert', 'delete', 'sync')
CAMSIS_INSERT_IMPORT_MODES = ('insert', 'upsert', 'sync')
CAMSIS_UPDATE_IMPORT_MODES = ('update', 'upsert', 'sync')
CAMSIS_DELETE_IMPORT_MODES = ('delete', 'sync')


def import_camsis_data(camsis_import_mode, settings, *, dry_run=False):
    """
    Synchronise a set of applicants with Camsis.
    """

    if camsis_import_mode not in CAMSIS_IMPORT_MODES:
        LOG.error(f'The camsis import mode must be one of: {CAMSIS_IMPORT_MODES}')
        sys.exit(1)

    settings = settings.get('camsis')

    min_expected_application_count = settings.get(
        'min_expected_application_count',
        DEFAULT_MIN_EXPECTED_APPLICATION_COUNT
    )

    if settings is None:
        LOG.error('The camsis setting must be provided.')
        sys.exit(1)

    # Configure the SMI api.
    smi_settings = settings.get('source', {})
    smi_url = smi_settings.get('url')
    smi_queries = smi_settings.get('queries', [])
    smi_headers = smi_settings.get('headers', {})

    if smi_url is None:
        LOG.error('The smi.url setting must be provided.')
        sys.exit(1)

    def do_patch(id, patch):
        """
        `patch` is used to update application `id` unless in `dry_run` mode.
        """
        patch_with_id = {**patch, 'id': id}
        if dry_run:
            LOG.info(f'Application {id} would be updated. Updated fields: '
                     f'{get_field_names(patch)}')
        else:
            LOG.info(f'Updating application {id}. Updated fields: {get_field_names(patch)}')
            application_url = urljoin(smi_url, f'{id}/')
            response = requests.patch(application_url, json=patch_with_id, headers=smi_headers)
            if not response.ok:
                LOG.error(response.text)

    def do_post(insert):
        """
        `post` is used to create an application unless in `dry_run` mode.
        """
        if dry_run:
            LOG.info(f'Application {insert.get("id")} would be created with fields: '
                     f'{get_field_names(insert)}')
        else:
            LOG.info(f'Creating application {insert.get("id")} with fields: '
                     f'{get_field_names(insert)}')
            response = requests.post(smi_url, json=insert, headers=smi_headers)
            if not response.ok:
                LOG.error(response.text)

    def do_delete(id):
        """
        `delete` is used to delete an application `id` unless in `dry_run` mode.
        """
        if dry_run:
            LOG.info(f'Application {id} would be deleted')
        else:
            LOG.info(f'Deleting application {id}')
            application_url = urljoin(smi_url, f'{id}/')
            response = requests.delete(application_url, headers=smi_headers)
            if not response.ok:
                LOG.error(response.text)

    with timeout_checker(settings) as has_timed_out:

        # Fetch all the applications from SMI that we need to sync
        LOG.info('Fetching all smi applications')
        smi_applications = {}
        for query in smi_queries:
            for application in fetch_applications(url=smi_url, query=query, headers=smi_headers):
                smi_applications[application['id']] = application

        LOG.info('Fetched %s smi applications', len(smi_applications))

        # Configure the CamSIS api.
        camsis_settings = settings.get('provider', {})
        camsis_url = camsis_settings.get('url')
        camsis_username = camsis_settings.get('username')
        camsis_password = camsis_settings.get('password')
        camsis_filter = camsis_settings.get('filter')
        camsis_exclude = camsis_settings.get('exclude', [])

        if camsis_url is None:
            LOG.error('The provider.url setting must be provided.')
            sys.exit(1)

        # Fetch all the applicants from CamSIS that we need to sync

        kwargs = (
            {} if None in [camsis_username, camsis_password] else
            {'auth': HTTPBasicAuth(camsis_username, camsis_password)}
        )
        response = requests.get(camsis_url, stream=True, **kwargs)
        response.raise_for_status()

        response.raw.decode_content = True
        events = ElementTree.iterparse(response.raw)

        # Iterate over the CamSIS applicants and try to match them to the SMI
        matched = 0
        updated = 0
        inserted = 0
        deleted = 0
        for _, element in events:
            tag = tag_no_ns(element)
            if tag == 'total':
                LOG.info('Fetched %s CamSIS applications', element.text)
            elif tag == 'applicant':
                camsis_app_smi_format = convert_camsis_app(element)

                # Apply filter to remove applications that shouldn't be synchronised
                if not match_filter(camsis_filter, camsis_app_smi_format):
                    continue

                # Apply exclusion filter to remove applications that shouldn't be synchronised
                if match_filter(camsis_exclude, camsis_app_smi_format):
                    continue

                if camsis_app_smi_format['id'] in smi_applications:
                    # If there is a match, check for differences and decide if these differences
                    # should be synchronised or just warned about.

                    smi_app = smi_applications.pop(camsis_app_smi_format['id'])

                    matched += 1
                    if camsis_import_mode in CAMSIS_UPDATE_IMPORT_MODES:
                        sync = create_sync(
                            camsis_app_smi_format, smi_app
                        )
                        if sync:
                            # synchronise the application with CamSIS
                            updated += 1
                            do_patch(camsis_app_smi_format['id'], sync)

                elif camsis_import_mode in CAMSIS_INSERT_IMPORT_MODES:
                    # If there's no match, decide if this applicant should be inserted into the smi
                    insert = create_insert(camsis_app_smi_format)
                    if insert:
                        inserted += 1
                        do_post(insert)

            if has_timed_out():
                break

        if camsis_import_mode in CAMSIS_DELETE_IMPORT_MODES:
            # Only run deletion if the number of applications returned by CamSIS is above a
            # threshold, otherwise it indicates a problem with CamSIS and we don't want to lose
            # applicants' files
            valid_camsis_applications_count = inserted + matched
            if valid_camsis_applications_count >= min_expected_application_count:
                # Any applications remaining in this dictionary are ones that do not appear
                # in CamSIS
                for smi_app_id in smi_applications:
                    if has_timed_out():
                        break
                    deleted += 1
                    do_delete(smi_app_id)
            else:
                LOG.error(
                    f"CamSIS did not return enough data "
                    f"({valid_camsis_applications_count}/{min_expected_application_count}) "
                    f"for safe auto-deletion to occur, "
                    f"would have deleted {len(smi_applications)} applications"
                )

    LOG.info(f'Change summary - Inserted: {inserted}, Updated: {updated} of {matched} SMI matches,'
             f' Deleted: {deleted}')


def convert_camsis_app(applicant):
    """
    Converts a CamSIS applicant record (XML) to an object comparable with an SMI application.
    """
    camsis_app_smi_format = {}

    for el in applicant:
        field, convert = FIELD_MAPPINGS.get(tag_no_ns(el), (None, None))
        if field:
            camsis_app_smi_format[field] = convert(el.text)

    for field, calculate in CALCULATED_FIELD_MAPPINGS.items():
        camsis_app_smi_format[field] = calculate(camsis_app_smi_format)

    return camsis_app_smi_format


def match_filter(filter_list, camsis_app_smi_format):
    """
    Compare SMI application to entries in the supplied filter list. If the application matches
    any one entry in the list then return True, otherwise return False.
    """
    if filter_list is None:
        return True
    for filter_item in filter_list:
        all_matched = True
        for prop, value in filter_item.items():
            if (prop not in camsis_app_smi_format) or (camsis_app_smi_format[prop] != value):
                all_matched = False
                break
        if all_matched:
            return True
    return False


def create_sync(camsis_app_smi_format, smi_app):
    """
    Looks for any differences between key fields in the CamSIS and SMI records and decides whether
    or not they should be synchronised. A dictionary of fields to synchronise is returned.
    """
    sync = {}
    for field, value in camsis_app_smi_format.items():
        if field not in NEVER_CREATE_OR_SYNC:
            nested_field = field.split('.')
            if camsis_app_smi_format.get(field) != get_nested(smi_app, nested_field):
                if field in ALWAYS_SYNC or (
                    field in SYNC_UNLESS_IN_TARGET_POOLS and
                    camsis_app_smi_format.get(POOL_TYPE_FIELD) not in TARGET_POOLS
                ):
                    set_nested(sync, nested_field, camsis_app_smi_format.get(field))

    return sync


def create_insert(camsis_app_smi_format):
    """
    Pick the CamSIS fields used by the SMI when creating a new applicant
    """
    insert = {}

    # Set the default field values - this only adds fields known to not be in CamSIS
    for field, value in INSERT_DEFAULT_FIELD_VALUES.items():
        insert[field] = value

    for field, value in camsis_app_smi_format.items():
        if field not in NEVER_CREATE_OR_SYNC:
            nested_field = field.split('.')
            set_nested(insert, nested_field, camsis_app_smi_format.get(field))

    return insert


def tag_no_ns(element):
    """Returns the name of the element without the namespace."""
    _, _, tag = element.tag.rpartition('}')
    return tag


def get_nested(dict, keys, default=None):
    """Gets a value from a nested dictionary"""
    try:
        return reduce(operator.getitem, keys, dict)
    except KeyError:
        return default


def set_nested(dict, keys, value):
    """Sets a value in a nested dictionary, creating any missing dictionaries"""
    current = dict
    for key in keys[:-1]:
        if key not in current:
            current[key] = {}
        current = current[key]

    current[keys[-1]] = value


def get_field_names(obj, separator='.'):
    """
    Gets a list of nested field names
    Nested fields are returned as 'parentFieldName.childFieldName'
    """
    result = []
    for field, value in obj.items():
        result.append(field)
        if isinstance(value, collections.abc.Mapping):
            result.extend([f"{field}{separator}{subfield}" for subfield in get_field_names(value)])
    return result
