import logging
import os

from flask import Flask, request, abort
import ugraddrivesync
import ugraddrivesync.camsis
import ugraddrivesync.eoiml
import ugraddrivesync.pmos
import ugraddrivesync.pooldrive
import ugraddrivesync.pooldrive.folders
import ugraddrivesync.pooldrive.permissions
import ugraddrivesync.pooldrive.validator
from ugraddrivesync.qualtrics.sync import sync_with_google_drive as qualtrics_sync_with_gdrive
from ugraddrivesync.capo import sync_with_google_drive as capo_sync_with_gdrive

# Configure logging. Reduce spammy-ness of the Google modules
logging.basicConfig(level=logging.INFO)
logging.getLogger('googleapiclient.discovery_cache').setLevel(logging.ERROR)

LOG = logging.getLogger(__name__)

app = Flask(__name__)


@app.route('/camsis/insert', methods=['POST'])
def camsis_insert():
    """
    Create applicants in the SMI based on the CamSIS feed.
    """
    ugraddrivesync.camsis.import_camsis_data(
        'insert',
        ugraddrivesync.load_settings(_get_jobspec_urls())
    )
    return {'status': 'ok'}


@app.route('/camsis/update', methods=['POST'])
def camsis_update():
    """
    Update applicant state in the SMI based on the CamSIS feed.
    """
    ugraddrivesync.camsis.import_camsis_data(
        'update',
        ugraddrivesync.load_settings(_get_jobspec_urls())
    )
    return {'status': 'ok'}


@app.route('/camsis/upsert', methods=['POST'])
def camsis_upsert():
    """
    Update or create applicant state in the SMI based on the CamSIS feed.
    """
    ugraddrivesync.camsis.import_camsis_data(
        'upsert',
        ugraddrivesync.load_settings(_get_jobspec_urls())
    )
    return {'status': 'ok'}


@app.route('/camsis/delete', methods=['POST'])
def camsis_delete():
    """
    Delete applicants from the SMI if they are missing in CamSIS feed.
    """
    ugraddrivesync.camsis.import_camsis_data(
        'delete',
        ugraddrivesync.load_settings(_get_jobspec_urls())
    )
    return {'status': 'ok'}


@app.route('/camsis/sync', methods=['POST'])
def camsis_sync():
    """
    Synchronise the applicants in the SMI with the CamSIS feed.
    """
    ugraddrivesync.camsis.import_camsis_data(
        'sync',
        ugraddrivesync.load_settings(_get_jobspec_urls())
    )
    return {'status': 'ok'}


@app.route('/capo/sync', methods=['POST'])
def capo_sync():
    """
    Upload CAPO documents to the Google Drives.

    """
    capo_sync_with_gdrive(ugraddrivesync.load_settings(_get_jobspec_urls()))
    return {'status': 'ok'}


@app.route('/eoiml/update', methods=['POST'])
def eoiml_update():
    """
    Update the "Expression of Interest" Google spreadsheet.

    """
    ugraddrivesync.eoiml.update(ugraddrivesync.load_settings(_get_jobspec_urls()))
    return {'status': 'ok'}


@app.route('/pmos/import/<pool_type>', methods=['POST'])
def pmos_import(pool_type):
    """
    Import the "Poolside Meeting Outcome Spreadsheets" (Google spreadsheets) from SMI data.

    """
    _validate_pool_type(pool_type)

    try:
        ugraddrivesync.pmos.import_smi_data(
            ugraddrivesync.load_settings(_get_jobspec_urls()), pool_type
        )
    except ugraddrivesync.ProcessTimeout:
        pass
    return {'status': 'ok'}


@app.route('/pmos/export/<pool_type>', methods=['POST'])
def pmos_export(pool_type):
    """
    Export the "Poolside Meeting Outcome Spreadsheets" (Google spreadsheets) to SMI data.

    """
    _validate_pool_type(pool_type)

    ugraddrivesync.pmos.export_smi_data(
        ugraddrivesync.load_settings(_get_jobspec_urls()), pool_type
    )
    return {'status': 'ok'}


@app.route('/pooldrive/update', methods=['POST'])
def pooldrive_update():
    """
    Update the Google Drive holding summer pool applicants.

    """
    ugraddrivesync.pooldrive.folders.update(
        ugraddrivesync.load_settings(_get_jobspec_urls())
    )
    return {'status': 'ok'}


@app.route('/pooldrive/validate', methods=['POST'])
def pooldrive_validate():
    """
    Validate the Google Drive holding applicants.

    """
    ugraddrivesync.pooldrive.validator.validate(
        ugraddrivesync.load_settings(_get_jobspec_urls())
    )
    return {'status': 'ok'}


@app.route('/pooldrive/permissions/update', methods=['PATCH'])
def pooldrive_permissions_update():
    """
    Update the Google Drive holding summer pool applicants.

    """
    body = request.get_json(force=True)
    group_permissions = []
    for group in body.get('groupPermissions', []):
        group_name = group.get('groupName', None)
        access_level = group.get('accessLevel', None)
        LOG.info('Request to modify group "%s" to access level "%s"', group_name, access_level)
        if group_name is not None and access_level is not None:
            group_permissions.append(dict(
                group_name=group_name,
                access_level=access_level,
            ))
        else:
            LOG.warning('Entry "groupPermissions" in request body does not have required' +
                        '"groupName" and "accessLevel" properties')

    ugraddrivesync.pooldrive.permissions.update(
        ugraddrivesync.load_settings(_get_jobspec_urls()),
        group_permissions=group_permissions
    )
    return {'status': 'ok'}


@app.route('/qualtrics/sync', methods=['POST'])
def qualtrics_sync():
    """
    Upload PDFs of the qualtrics survey responses to the Google Drives.

    """
    qualtrics_sync_with_gdrive(ugraddrivesync.load_settings(_get_jobspec_urls()))
    return {'status': 'ok'}


def _validate_pool_type(pool_type):
    """
    Raises 404 if the pool type is invalid

    """
    if pool_type not in ('winter', 'summer'):
        abort(404, description=f'{pool_type} is not a valid pool type')


def _get_jobspec_urls():
    """
    Examine the JOBSPEC_URLS environment variable and return a list of URLs which form the jobspec
    configuration. If the environment variable is missing or empty, raise a RuntimeError.

    """
    jobspec_urls = os.environ.get('JOBSPEC_URLS', '').split(',')
    if len(jobspec_urls) == 0:
        LOG.error('No jobspecs defined. Set JOBSPEC_URLS environment variable')
        raise RuntimeError('No jobspecs')
    return jobspec_urls
