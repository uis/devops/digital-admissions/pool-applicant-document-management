import logging
import sys

import requests
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError

from .. import (
    COMMON_PARAMS, fetch_applications, FILES_LIST_PAGE_SIZE, get_applicant_folders,
    get_credentials, timeout_checker
)

LOG = logging.getLogger(__name__)

# Google Drive API scopes
SCOPES = [
    'https://www.googleapis.com/auth/drive',
    'https://www.googleapis.com/auth/drive.file',
    'https://www.googleapis.com/auth/drive.appfolder',
]
# Default minimum number of applicants expected to be returned by the SMI
DEFAULT_MIN_EXPECTED_APPLICATION_COUNT = 1000


def update(settings, *, dry_run=False, delete_unmatched_folders=True):
    credentials = get_credentials(settings, scopes=SCOPES)
    service = build('drive', 'v3', credentials=credentials)
    pooldrive_settings = settings.get('pooldrive')

    if pooldrive_settings is None:
        LOG.error('The pooldrive setting must be provided.')
        sys.exit(1)

    # Configure Google Drive settings
    google_drive_settings = pooldrive_settings.get('google_drive', {})
    default_drive_id = google_drive_settings.get('drive_id')
    college_settings_list = google_drive_settings.get('colleges', [])
    per_college_settings = {
        record['code']: record
        for record in college_settings_list
    }
    per_college_filters = {
        record['code']: record['filter']
        for record in college_settings_list if 'filter' in record
    }

    # A function which returns the drive id and folder id for a given college
    def college_drive_and_folder_id(college_code):
        record = per_college_settings.get(college_code, {})
        drive_id = record.get('drive_id', default_drive_id)
        folder_id = record.get('folder_id', drive_id)
        if drive_id is None:
            LOG.error('No google drive id configured for college %s', college_code)
            sys.exit(1)
        return drive_id, folder_id

    # Configure applicant source
    source_settings = pooldrive_settings.get('source', {})
    source_url = source_settings.get('url')
    source_queries = source_settings.get('queries', [])
    source_headers = source_settings.get('headers', {})

    min_expected_application_count = source_settings.get(
        'min_expected_application_count',
        DEFAULT_MIN_EXPECTED_APPLICATION_COUNT
    )

    if source_url is None:
        LOG.error('The source.url setting must be provided.')
        sys.exit(1)

    with timeout_checker(pooldrive_settings) as has_timed_out:

        # Fetch all the applications we need to sync
        LOG.info('Fetching all applications')
        all_applications = []
        for query in source_queries:
            all_applications.extend(
                fetch_applications(url=source_url, query=query, headers=source_headers)
            )

        unique_applications = list(
            {
                obj["camsisApplicationNumber"]: obj
                for obj in all_applications
            }.values()
        )
        if len(unique_applications) != len(all_applications):
            LOG.warning('Applications fetched are not unique.'
                        f'{len(unique_applications)} are unique, {len(all_applications)} total.')
            LOG.warning('Switching to use unique ones.')
            all_applications = unique_applications

        LOG.info('Fetched %s applications', len(all_applications))

        # Arrange applications into a dictionary keyed by (drive_id, folder_id) tuples, i.e. the
        # destination of the applications.
        applications_by_destination = {}
        # Make sure the default drive/folder has at least an empty list
        applications_by_destination.setdefault(college_drive_and_folder_id('DEFAULT'), [])
        # Make sure that every college drive/folder has at least an empty list
        for code in per_college_settings:
            destination = college_drive_and_folder_id(code)
            applications_by_destination.setdefault(destination, [])
        # Add every application to the appropriate list in applications_by_destination
        for application in all_applications:
            college_preference = application['collegePreference']
            for filter_college, filter_list in per_college_filters.items():
                for filter_spec in filter_list:
                    all_matched = True
                    for filter_property, filter_value in filter_spec.items():
                        if application[filter_property] != filter_value:
                            all_matched = False
                            break
                    if all_matched:
                        college_preference = filter_college
                        break
            destination = college_drive_and_folder_id(college_preference)
            app_list = applications_by_destination.setdefault(destination, [])
            app_list.append(application)

        LOG.info('Applications will be added to %s destinations', len(applications_by_destination))

        if dry_run:
            LOG.info('Dry run mode enabled. No changes will be made to the drive.')

        # Pre-cache all of the applicant folders, allowing us to later check if we need to move a
        # folder between drives
        applicant_folders_by_ucas = {}
        for destination, applications in applications_by_destination.items():
            search_drive_id, search_folder_id = destination

            LOG.info('Locally caching existing folders')
            for applicant_folder in get_applicant_folders(
                service.files(),
                search_drive_id,
                search_folder_id,
                has_timed_out
            ):
                if 'appProperties' not in applicant_folder:
                    LOG.warning(f"unrecognised folder: {applicant_folder['id']} "
                                f"({applicant_folder['webViewLink']})")
                    continue

                ucas_number = applicant_folder['appProperties']['ucas_number']
                applicant_folders_by_ucas.setdefault(ucas_number, [])
                applicant_folders_by_ucas[ucas_number].append(applicant_folder)

                unique_applicant_folders = list(
                    {
                        obj['id']: obj
                        for obj in applicant_folders_by_ucas[ucas_number]
                    }.values()
                )
                if len(unique_applicant_folders) != len(applicant_folders_by_ucas[ucas_number]):
                    LOG.warning(f'Applicant folders are not unique for applicant {ucas_number}.'
                                f'{len(unique_applicant_folders)} are unique, '
                                f'{len(applicant_folders_by_ucas[ucas_number])} total.')
                    LOG.warning('Switching to use unique ones.')
                    applicant_folders_by_ucas[ucas_number] = unique_applicant_folders

        successes = deleted = failures = total = 0

        for destination, applications in applications_by_destination.items():
            search_drive_id, search_folder_id = destination
            LOG.info(
                'There should be %s applications in drive %s, folder %s',
                len(applications), search_drive_id, search_folder_id
            )

            # try to match applicants with existing folders
            for application in applications:
                # Get the external resource endpoint URL for creating a new external resource
                external_resources_url = application['externalResourcesListUrl']

                # This gets updated with the web-view link for the application folder. How this
                # gets determined depends on whether the application folder exists in the first
                # place or whether it gets returned by Google when the folder is created.
                folder_web_view_link = None

                metadata = create_metadata(application)
                ucas_number = application['candidate']['ucasPersonalId']
                total += 1
                try:
                    if ucas_number in applicant_folders_by_ucas:
                        # Ideally there would only be one folder per applicant but
                        # this has sometimes not been true so find the "best" folder
                        applicant_folders = applicant_folders_by_ucas.pop(ucas_number)
                        applicant_folder = pop_best_folder(applicant_folders, search_folder_id)

                        # Move all the duplicate folder(s)'s content into the main applicant folder
                        for applicant_folder_duplicate in applicant_folders:
                            duplicate_folder_id = applicant_folder_duplicate['id']
                            LOG.info(
                                f"Moving contents of folder {duplicate_folder_id} "
                                f"to {applicant_folder['id']}"
                            )
                            move_folder_content(
                                service.files(),
                                duplicate_folder_id,
                                applicant_folder['id']
                            )
                            try:
                                delete_folder(
                                    service.files(),
                                    duplicate_folder_id,
                                    ucas_number,
                                    dry_run
                                )
                                deleted += 1
                            except HttpError as error:
                                LOG.error("Delete failed for: %s (%s)", duplicate_folder_id, error)
                                failures += 1

                        # Extract the web view link for this folder.
                        folder_web_view_link = applicant_folder['webViewLink']

                        id = applicant_folder.pop('id')
                        # We only care if any of the metadata fields have changed, not anything
                        # else from the File resource.

                        changed_metadata_fields = [
                            k for k in metadata.keys() if applicant_folder[k] != metadata[k]
                        ]

                        is_meta_changed = len(changed_metadata_fields) > 0

                        if is_meta_changed:
                            LOG.debug("updating folder: %s", ucas_number)
                            if dry_run:
                                LOG.info(
                                    'Would update folder %s with changed metadata fields %r', id,
                                    changed_metadata_fields
                                )
                            else:
                                LOG.info(
                                    'Updating folder %s with changed metadata fields %r', id,
                                    changed_metadata_fields
                                )
                                response = service.files().update(
                                    fileId=id, body=metadata, **COMMON_PARAMS).execute()

                        is_folder_moved = search_folder_id not in applicant_folder['parents']

                        if is_folder_moved:
                            if dry_run:
                                LOG.info(
                                    f'Applicant folder {ucas_number} is not in the correct '
                                    f'location. Would move folder {id} to {search_folder_id}. '
                                    f'Current location {applicant_folder["parents"]}'
                                )
                            else:
                                LOG.info(
                                    f'Applicant folder {ucas_number} is not in the correct '
                                    f'location. Moving folder {id} to {search_folder_id}. '
                                    f'Current location {applicant_folder["parents"]}'
                                )
                                response = service.files().update(
                                    fileId=id,
                                    addParents=search_folder_id,
                                    removeParents=','.join(applicant_folder.get('parents')),
                                    fields='id, parents',
                                    **COMMON_PARAMS).execute()

                        if is_meta_changed or is_folder_moved:
                            successes += 1

                    else:
                        # no match so we create a new folder
                        LOG.debug("creating folder: %s", ucas_number)
                        metadata_for_create = {
                            'mimeType': 'application/vnd.google-apps.folder',
                            'parents': [search_folder_id],
                            **metadata
                        }
                        if dry_run:
                            LOG.info('Would create folder for: %s', ucas_number)
                        else:
                            LOG.info('Creating folder for: %s', ucas_number)
                            response = service.files().create(
                                body=metadata_for_create, fields='webViewLink',
                                **COMMON_PARAMS
                            ).execute()

                            # Extract the web view link for this folder.
                            folder_web_view_link = response['webViewLink']
                        successes += 1

                    if not dry_run and folder_web_view_link is None:
                        # If we're not in dry run mode but we didn't get a web link for the
                        # application folder, log an error.
                        LOG.error(
                            'No web view link for application %s',
                            application['camsisApplicationNumber']
                        )
                    elif not dry_run:
                        # Ensure the application has an external resource set with the appropriate
                        # link.
                        expected_external_resource = {
                            'externalUrl': folder_web_view_link,
                            'description': 'Applicant Files on Google Drive',
                        }

                        current_external_resource = (
                            None
                            if len(application['externalResources']) == 0
                            else application['externalResources'][0]
                        )

                        if current_external_resource is None:
                            # If there is no resource, create one
                            LOG.info(
                                'Creating external resource for application %s',
                                application['camsisApplicationNumber']
                            )
                            LOG.info('POST-ing to %s', external_resources_url)
                            resp = requests.post(
                                external_resources_url, headers=source_headers,
                                data={
                                    'applicationId': application['camsisApplicationNumber'],
                                    **expected_external_resource,
                                }
                            )
                            resp.raise_for_status()
                        elif any(
                                current_external_resource[k] != v
                                for k, v in expected_external_resource.items()):
                            # If the resource is wrong, fix it.
                            LOG.info(
                                'Updating external resource for application %s',
                                application['camsisApplicationNumber']
                            )
                            LOG.info('PATCH-ing %s', current_external_resource['url'])
                            resp = requests.patch(
                                current_external_resource['url'], headers=source_headers,
                                data=expected_external_resource
                            )
                            resp.raise_for_status()
                except HttpError as error:
                    LOG.warning("failed for: %s (%s)", ucas_number, error)
                    failures += 1

                if has_timed_out():
                    break

        # Only run deletion if the number of applications returned by SMI is above a threshold,
        # otherwise it indicates a problem with SMI and we don't want to lose applicants' files
        if len(all_applications) < min_expected_application_count:
            # Disable deletion
            delete_unmatched_folders = False
            LOG.error(
                f"SMI did not return enough data "
                f"({len(all_applications)}/{min_expected_application_count}) "
                f"for safe auto-deletion to occur"
            )

        # Any folders left in the list are not in the SMI and should be deleted or warned about
        for (ucas_number, applicant_folders) in applicant_folders_by_ucas.items():

            if has_timed_out():
                break

            for applicant_folder in applicant_folders:
                app_folder_id = applicant_folder['id']
                if delete_unmatched_folders:
                    try:
                        delete_folder(service.files(), app_folder_id, ucas_number, dry_run)
                        deleted += 1
                    except HttpError as error:
                        LOG.error("Delete failed for: %s (%s)", applicant_folder, error)
                        failures += 1
                else:
                    LOG.warning(f'Unmatched folder {app_folder_id} for applicant {ucas_number}')

    LOG.info("%s total applications", total)
    LOG.info("%s successful creates/updates", successes)
    LOG.info("%s successful deletes", deleted)
    LOG.info("%s failed creates/updates/deletes", failures)


def pop_best_folder(possible_folders, expected_parent_folder):
    """
    Sometimes unexpectedly applicants end up with multiple folders
    (because they have appeared in the SMI twice)
    We want to select the correct folder to update.
    This function attempts to find the applicant folder that is in the correct college folder
    """
    # If the same folder has been fetched multiple times, don't handle as if there are multiple.
    unique_folders = list({obj["id"]: obj for obj in possible_folders}.values())
    if len(possible_folders) != len(unique_folders):
        LOG.warning(
            f'Multiple identical folders found: {[folder["id"] for folder in possible_folders]}'
        )
        possible_folders.clear()
        possible_folders.extend(unique_folders)

    # If there's only one folder then, great, everything is working as expected
    if len(possible_folders) == 1:
        return possible_folders.pop()

    # Otherwise, attempt to find the best folder
    result = None
    for (i, folder) in enumerate(possible_folders):
        if expected_parent_folder in folder['parents']:
            result = possible_folders.pop(i)
            break

    # We failed to find a best, just pop the last
    if result is None:
        result = possible_folders.pop()

    LOG.warning(f'Applicant {result["appProperties"]["ucas_number"]} has multiple folders, '
                f'selecting {result["id"]} as the main folder. '
                f'Other folders: {[folder["id"] for folder in possible_folders]}.')
    return result


def create_metadata(application):
    """
    Converts an Application resource to metadata for creating a folder. Note the id's are
    stored as [custom file properties](https://developers.google.com/drive/api/v3/properties).
    """
    camsis_id = application['camsisApplicationNumber']
    applicant_name = application['candidate']['displayName']
    applicant_forenames = application['candidate']['forenames']
    applicant_surname = application['candidate']['lastName']
    ucas_number = application['candidate']['ucasPersonalId']
    subject_code = application['subject']

    description_fields = {
        'UCAS': ucas_number,
        'USN': camsis_id,
        'Subject code': subject_code,
        'Subject description': application['subjectDescription'],
    }
    description_fields_str = ', '.join(f'{k}: {v}' for k, v in description_fields.items())

    return {
        'name': f'{subject_code} - {applicant_surname}, {applicant_forenames} - {ucas_number}',
        'appProperties': {'ucas_number': ucas_number, 'camsis_id': camsis_id},
        # The main reason for setting the description is to provide the UI capability of searching
        # by id.
        'description': f'Application documents for "{applicant_name}" ({description_fields_str})',
    }


def move_folder_content(files_service, source_folder_id, destination_folder_id):
    """
    Moves files and folders from within `source_folder` to `destination_folder`
    """
    page_token = None
    while True:
        LOG.debug('Retrieving page of files')
        response = files_service.list(
            # NOTE that we don't strictly need the 2nd two clauses
            q=f"((trashed = false) and '{source_folder_id}' in parents)",
            fields='nextPageToken, files(id)',
            pageToken=page_token,
            corpora='allDrives',
            includeItemsFromAllDrives=True,
            pageSize=FILES_LIST_PAGE_SIZE,
            **COMMON_PARAMS
        ).execute()
        for file in response.get('files', []):
            response = files_service.update(
                fileId=file['id'],
                addParents=destination_folder_id,
                removeParents=source_folder_id,
                fields='id, parents',
                **COMMON_PARAMS).execute()
            page_token = response.get('nextPageToken', None)
        if page_token is None:
            break


def delete_folder(files_service, folder_id, applicant_ucas_id, dry_run):
    if dry_run:
        LOG.info(f'Would delete folder {folder_id} for applicant {applicant_ucas_id}')
    else:
        LOG.info(f'Deleting folder {folder_id} for applicant {applicant_ucas_id}')
        files_service.update(fileId=folder_id, body={'trashed': True},  **COMMON_PARAMS).execute()
