#!/usr/bin/env python3
"""
Synchronise a set of folders in a Google Shared Drive with a list of pool applicants. The script is
configured with one or more urls for YAML job spec files. `jobspec.yml.example` is provided as
documentation of the possible settings and their acceptable values. `file`, `https`, and `gs` urls
are supported.

Usage:
    ugraddrivesync [--dry-run] camsis (insert|update|upsert|delete|sync) <job-spec-url>...
    ugraddrivesync [--dry-run] capo sync <job-spec-url>...
    ugraddrivesync [--dry-run] eoiml update <job-spec-url>...
    ugraddrivesync [--dry-run] pmos (import|export) (winter|summer) <job-spec-url>...
    ugraddrivesync [--dry-run] pooldrive (update|validate) <job-spec-url>...
    ugraddrivesync [--dry-run] pooldrive permissions update
      [--group-permission=<group>=<level>]... <job-spec-url>...
    ugraddrivesync [--dry-run] qualtrics sync <job-spec-url>...
    ugraddrivesync (-h | --help)

Options:
    -h, --help                          Show a brief usage summary.

    --group-permission=<group>=<level>  Assign an access level to the Google
                                        Drives for a group of users

    --dry-run                           Print what changes would be made but
                                        don't make them.

The <job-spec-url> positional arguments should be URLs to a job specification file. See the
"jobspec.yml.example" file in the source distribution for the format. If the URL lacks a scheme,
the "file" scheme is assumed. Supported schemes are "https", "file" and "gs". The "gs" scheme
allows configuration contained within a Google Storage bucket to be specified in the same way as
with the "gsutil" command.

User groups can be specified in the job specification files.

If multiple job specifications are provided, the specifications are *deep merged*.

Commands:

    camsis insert

        Create applicants which appear in CamSIS but that do not already exist in the SMI

    camsis update

        Synchronise a set of applicants with Camsis.

    camsis upsert

        Create or update SMI applicants from the CamSIS data

    camsis delete

        Delete SMI applicants if they don't appear in the CamSIS data

    camsis sync

        Synchronise the SMI applicant records with CamSIS feed. Creating, updating, or deleting.

    capo sync

        Downloads new CAPO documents from CamSIS and uploads in the appopriate Google Drive folder
        for an applicant.

    eoiml update

        Synchronise a set of applicants with the "Expression of Interest" Google spreadsheet.

    pmos import summer

        Synchronise a set of applicants with a set of "Poolside Meeting Outcome Spreadsheet"
        Google spreadsheets for the summer pool.

    pmos export summer

        Synchronise the data inputted into each "Poolside Meeting Outcome Spreadsheet" Google
        spreadsheet with the SMI for the summer pool. Changes in the sheet result in the creation
        of pool outcomes in the SMI.

    pooldrive update

        Synchronise one or more Google Drives with the currently pooled applicants.

    pooldrive validate

        Validate the applicant file names and structure within Google Drive.

    pooldrive permissions update

        Set permissions on one or more Google Drives for groups of users.

    qualtrics sync

        Collects a list of survey responses from Qualtrics, converts them to pdf,
        and uploads in the appropriate Google Drive folder for an applicant (or a default on if a
        matching applicant cannot be found)

"""

import docopt
import logging
import logging.config
import pkg_resources

from ugraddrivesync import load_settings
from ugraddrivesync import camsis, capo, eoiml, pmos
from ugraddrivesync.pooldrive import folders as pooldrive_folders
from ugraddrivesync.pooldrive import permissions as pooldrive_permissions
from ugraddrivesync.pooldrive import validator as pooldrive_validator
from ugraddrivesync.qualtrics import sync as qualtrics_sync

logging.config.fileConfig(pkg_resources.resource_filename(__name__, 'logging.conf'))

LOG = logging.getLogger(__name__)


def main():
    opts = docopt.docopt(__doc__)

    # read the settings
    settings = load_settings(opts['<job-spec-url>'])

    kwargs = {'dry_run': opts['--dry-run']}

    # Run the appropriate command
    if opts['camsis'] and opts['insert']:
        camsis.import_camsis_data('insert', settings, **kwargs)
    elif opts['camsis'] and opts['update']:
        camsis.import_camsis_data('update', settings, **kwargs)
    elif opts['camsis'] and opts['upsert']:
        camsis.import_camsis_data('upsert', settings, **kwargs)
    elif opts['camsis'] and opts['delete']:
        camsis.import_camsis_data('delete', settings, **kwargs)
    elif opts['camsis'] and opts['sync']:
        camsis.import_camsis_data('sync', settings, **kwargs)
    elif opts['capo'] and opts['sync']:
        capo.sync_with_google_drive(settings, **kwargs)
    elif opts['eoiml'] and opts['update']:
        eoiml.update(settings, **kwargs)
    elif opts['pmos']:
        pool_type = "summer" if opts['summer'] else "winter"
        if opts['import']:
            pmos.import_smi_data(settings, pool_type, **kwargs)
        elif opts['export']:
            pmos.export_smi_data(settings, pool_type, **kwargs)
    elif opts['pooldrive'] and opts['permissions'] and opts['update']:
        group_opts = [x.split('=') for x in opts['--group-permission']]
        group_permissions = [
            dict(group_name=group_name, access_level=access_level)
            for group_name, access_level in group_opts
        ]
        pooldrive_permissions.update(
            settings, group_permissions=group_permissions, **kwargs
        )
    elif opts['pooldrive'] and opts['update']:
        pooldrive_folders.update(settings, **kwargs)
    elif opts['pooldrive'] and opts['validate']:
        pooldrive_validator.validate(settings, **kwargs)
    elif opts['qualtrics'] and opts['sync']:
        qualtrics_sync.sync_with_google_drive(settings, **kwargs)


if __name__ == '__main__':
    main()
