#!/usr/bin/env sh
exec gunicorn -b "0.0.0.0:$PORT" --timeout 900 app:app
