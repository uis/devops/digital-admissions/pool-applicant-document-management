import logging
import re
import sys

from googleapiclient.discovery import build

from .. import COMMON_PARAMS, get_credentials

LOG = logging.getLogger(__name__)

FOLDER_MIME_TYPE = 'application/vnd.google-apps.folder'

# Google Drive API scopes
SCOPES = [
    'https://www.googleapis.com/auth/drive',
    'https://www.googleapis.com/auth/drive.file',
    'https://www.googleapis.com/auth/drive.appfolder',
]

# Files list page size (this is also the maximum)
FILES_LIST_PAGE_SIZE = 1000

# Regexes used to identify possible PIDs
REGEX_POSSIBLE_PID = [
    r'[\d-]+',
    r'[\d-]+\D[\d-]+',
    r'[\d-]+\D([\d-]+\D[\d-]+)'
]
REGEX_PID_FORMAT = r'^\d{10}$'
REGEX_NEARLY_PID_LENGTH = r'^.{9,11}$'
REGEX_NEARLY_PID_CHARS = r'^\d+\D?\d+$'

# Possible error message when checking file name for possible PIDs.
# Indexed by error code
FILE_NAME_ERROR_MSGS = dict(
    PID_MISMATCH_FOLDER="File name contains PID but doesn't match the applicant folder PID",
    PID_PROBABLE_MISTYPE="File name contains value that is probably a PID mistype",
)

# A description for the default college drive
DEFAULT_FOLDER_DESCRIPTION = 'Default College Drive'


class FileInMultipleLocationsError(Exception):
    pass


class ValidationError(Exception):
    pass


def validate(settings, *, dry_run=False):
    """
    Validates applicant files and folder in google drive
    """
    credentials = get_credentials(settings, scopes=SCOPES)
    files_service = build('drive', 'v3', credentials=credentials).files()

    # If the process is configured with college applicant repos as sub-folders of drives instead of
    # the drive themselves (with potentially all of them in a single drive), then we cache files
    # here (by drive in case there is more than 1 drive) to avoid unnecessary calls.
    files_cached_by_drive = {}

    def get_all_files_and_folders(drive_id, folder_id):
        """
        Get all files and folders in a drive (or a folder, if different from the drive). If we are
        querying a folder, we still have to retrieve everything from the drive and filter as there
        is no way to directly query all descendants of a folder. To minimise the number of calls,
        we cache the files by drive.
        """

        drive_files_and_folders = get_all_files_and_folders_for_drive(files_service, drive_id)

        if drive_id == folder_id:
            # simple case of a college applicant repo per drive
            return drive_files_and_folders

        nonlocal files_cached_by_drive

        # populate the cache, if we haven't already
        if drive_id not in files_cached_by_drive:
            files_cached_by_drive[drive_id] = drive_files_and_folders

        def is_contained_by_folder(file):
            """
            A recursive closure used to determine if a file is a descendant of `folder_id`.
            Note that multiple parents could give unpredictable results.
            """
            for parent_id in file['parents']:
                if parent_id == folder_id:
                    return True
                elif parent_id == drive_id:
                    return False
                else:
                    return is_contained_by_folder(files_cached_by_drive[drive_id][parent_id])

        # filter the drive files to return only descendants of folder_id
        return {
            file['id']: file
            for file in files_cached_by_drive[drive_id].values()
            if is_contained_by_folder(file)
        }

    pooldrive_settings = settings.get('pooldrive')

    if pooldrive_settings is None:
        LOG.error('The pooldrive setting must be provided.')
        sys.exit(1)

    # Configure Google Drive settings
    google_drive_settings = pooldrive_settings.get('google_drive', {})
    default_drive_id = google_drive_settings.get('drive_id')
    default_folder_id = google_drive_settings.get('folder_id', default_drive_id)

    # A `dict` of folder descriptions
    folder_id_to_description = {
        record.get('folder_id', record['drive_id']): record.get('description', record['code'])
        for record in google_drive_settings.get('colleges', [])
    }
    folder_id_to_description[default_folder_id] = DEFAULT_FOLDER_DESCRIPTION

    # create a list of all the searcheable drives (all college drives and the default drive)
    searchable_drives_and_folders = [(default_drive_id, default_folder_id)] + [
        (record['drive_id'], record.get('folder_id', record['drive_id']))
        for record in google_drive_settings.get('colleges', [])
    ]

    # Initialise variables for tracking the results of the check
    checked = 0
    applicant_folder_checked = 0
    other_folder_checked = 0
    file_checked = 0
    failed = 0
    applicant_folder_multiple_locations_failed = 0
    applicant_folder_moved_failed = 0
    applicant_folder_invalid_name_failed = 0
    folder_not_applicant_folder_failed = 0
    file_not_in_applicant_folder_failed = 0
    file_invalid_name_failed = 0

    # Search all of the drives' folders looking for invalid items
    for search_drive_id, search_folder_id in searchable_drives_and_folders:
        files_and_folders_by_id = get_all_files_and_folders(search_drive_id, search_folder_id)

        for file_id, file in files_and_folders_by_id.items():
            # Skip folders outside of the current search folder
            if not is_file_inside_folder(file, search_folder_id, files_and_folders_by_id):
                continue

            file_name = file['name']
            drive_description = folder_id_to_description[search_folder_id]
            checked += 1

            # Check folders - name and location
            if file['mimeType'] == FOLDER_MIME_TYPE:
                # Check if this is an applicant folder
                if 'appProperties' in file:
                    applicant_folder_checked += 1

                    # Check the location of the applicant folder
                    if len(file['parents']) > 1:
                        LOG.error(f'{drive_description}: Folder {file_id} "{file_name}" '
                                  f'appears to be in multiple locations: '
                                  f'{file["parents"]}')
                        failed += 1
                        applicant_folder_multiple_locations_failed += 1
                        continue
                    elif search_folder_id not in file['parents']:
                        LOG.error(f'{drive_description}: Folder {file_id} "{file_name}" '
                                  f'has been moved, it should be inside folder '
                                  f'{search_folder_id}. '
                                  f'Current location: {file.get("parents")}')
                        applicant_folder_moved_failed += 1
                        failed += 1
                        continue

                    # Check the name of the applicant folder
                    ucas_pid = file['appProperties']['ucas_number']
                    if not contains_ucas_pid(file['name'], ucas_pid):
                        LOG.error(f'{drive_description}: Folder {file_id} "{file_name}" '
                                  f'does not have a valid name, must contain UCAS PID '
                                  f'{ucas_pid}, it must have been renamed. '
                                  f'Current location: {file.get("parents")}')
                        applicant_folder_invalid_name_failed += 1
                        failed += 1
                        continue
                else:
                    other_folder_checked += 1

                    # If not an applicant folder then it should be inside applicant folder
                    applicant_folder = find_parent_applicant_folder(file, files_and_folders_by_id)

                    # Folders outside an applicant folder shouldn't exist
                    if applicant_folder is None:
                        LOG.error(f'{drive_description}: Folder {file_id} "{file_name}" '
                                  f'is not an applicant folder. '
                                  f'Current location: {file.get("parents")}')
                        folder_not_applicant_folder_failed += 1
                        failed += 1
                        continue

            # Check files - name
            else:
                file_checked += 1
                applicant_folder = find_parent_applicant_folder(file, files_and_folders_by_id)

                # Files without an applicant folder shouldn't exist
                if applicant_folder is None:
                    LOG.error(f'{drive_description}: File {file_id} "{file_name}" '
                              f'is not in an applicant folder. '
                              f'Current location: {file.get("parents")}')
                    file_not_in_applicant_folder_failed += 1
                    failed += 1
                    continue

                ucas_pid = applicant_folder['appProperties']['ucas_number']

                # Check the naming of the file
                result, msg = check_ucas_pid_like_values(file['name'], ucas_pid)
                if not result:
                    parent_file_names = [
                        files_and_folders_by_id[parent_id].get('name')
                        for parent_id in file.get('parents')
                    ]
                    LOG.error(f'{drive_description}: File {file_id} "{file_name}" '
                              f'does not have a valid file name -- {msg}. '
                              f'Current location: {file.get("parents")} {parent_file_names}')
                    file_invalid_name_failed += 1
                    failed += 1
                    continue

    LOG.info(f'Summary: {checked} files and folders checked, {failed} failed')
    LOG.info(f'Checked applicant folders: {applicant_folder_checked}')
    LOG.info(f'Checked other folders: {other_folder_checked}')
    LOG.info(f'Checked files: {file_checked}')
    LOG.info(f'Failed -- applicant folders in multiple locations: '
             f'{applicant_folder_multiple_locations_failed}')
    LOG.info(f'Failed -- applicant folders moved: '
             f'{applicant_folder_moved_failed}')
    LOG.info(f'Failed -- applicant folders with invalid names: '
             f'{applicant_folder_invalid_name_failed}')
    LOG.info(f'Failed -- folders not valid applicant folders: '
             f'{folder_not_applicant_folder_failed}')
    LOG.info(f'Failed -- files not in applicant folders: '
             f'{file_not_in_applicant_folder_failed}')
    LOG.info(f'Failed -- files with invalid names (PID check failed): '
             f'{file_invalid_name_failed}')

    if not dry_run and failed > 0:
        raise ValidationError("Validation Failed")


def get_all_files_and_folders_for_drive(files_service, drive_id):
    """
    Gets a dictionary of all files and folders from google drive for a particular drive,
    keyed by id.
    """
    files_by_id = {}
    page_token = None
    while True:
        LOG.debug('Retrieving page of files')
        response = files_service.list(
            q='trashed = false',
            fields='nextPageToken, '
                   'files(id, name, description, appProperties, webViewLink, mimeType, parents)',
            pageToken=page_token,
            corpora='drive',
            driveId=drive_id,
            includeItemsFromAllDrives=True,
            pageSize=FILES_LIST_PAGE_SIZE,
            **COMMON_PARAMS
        ).execute()
        for file in response.get('files', []):
            files_by_id[file['id']] = file
        page_token = response.get('nextPageToken', None)
        if page_token is None:
            break
    return files_by_id


def contains_ucas_pid(file_name, identifier):
    assert len(identifier) > 0
    return re.search(f'(\\b|_){re.escape(identifier)}(\\b|_)', file_name) is not None


def check_ucas_pid_like_values(file_name, folder_ucas_pid):
    """
    Checks for possible PID values in the passed file name, and verifies their values.

    Returns a tuple (result, msg).

    <result> is True if either:
    - the passed file name contains a valid PID, and doesn't contain any invalid PID-like values
    - the passed file name doesn't contain any possible PIDs

    <msg> is None if <result> is True, otherwise it contains a string that explains why the check
    failed.
    """
    # Get all possible PID values
    possible_pids = []
    for regex in REGEX_POSSIBLE_PID:
        possible_pids.extend(re.findall(regex, file_name))

    # Check each possible PID
    for possible_pid in possible_pids:
        # Remove hyphens which can be used to break up the PID
        possible_pid = possible_pid.replace('-', '')

        # If value is a valid PID format then it must match the applicant folder PID
        is_pid_format = re.match(REGEX_PID_FORMAT, possible_pid) is not None
        if is_pid_format:
            if possible_pid != folder_ucas_pid:
                return (False, FILE_NAME_ERROR_MSGS['PID_MISMATCH_FOLDER'])
        else:
            # If value is similar to a PID and isn't an exceptional case, then flag an error.
            # Exceptional cases are non-PID ID values used by some colleges.
            is_nearly_pid_length = re.match(REGEX_NEARLY_PID_LENGTH, possible_pid) is not None
            is_nearly_pid_chars = re.match(REGEX_NEARLY_PID_CHARS, possible_pid) is not None
            is_exception = possible_pid.startswith('2100') and len(possible_pid) == 9
            if is_nearly_pid_length and is_nearly_pid_chars and not is_exception:
                return (False, FILE_NAME_ERROR_MSGS['PID_PROBABLE_MISTYPE'])

    return (True, None)


def find_parent_applicant_folder(file, folders_by_id):
    """
    Searches the parent folders of file for the closest folder with applicant details
    Returns None if it doesn't find one
    Only returns a value if the file (or its parents) are within `inside_folder`
    """
    parents = file['parents']
    if len(parents) > 1:
        LOG.error(f'File {file["id"]} appears to be in multiple locations: {file["parents"]}')
        return None

    # Files should always have a parent, but it's nice to be safe
    if len(parents) == 0:
        # File doesn't have a parent applicant folder, we probably reached the root of the drive
        return None

    parent_id = parents[0]
    parent = folders_by_id.get(parent_id)

    if parent is None or parent == file:
        # File doesn't have a parent applicant folder, we probably reached the root of the drive
        return None
    elif 'appProperties' in parent:
        # Found the applicant folder
        return parent
    else:
        # Keep searching the parent tree
        return find_parent_applicant_folder(parent, folders_by_id)


def is_file_inside_folder(file, folder_id, folders_by_id):
    """
    Checks that a file (or folder) is a child (or nested child) of a folder
    """
    # If the provided file is the folder then that is good enough
    if file['id'] == folder_id:
        return True

    parents = file['parents']
    if len(parents) > 1:
        raise FileInMultipleLocationsError(f'File {file["id"]} has multiple parents')

    # Files should always have a parent, but it's nice to be safe
    if len(parents) == 0:
        # File doesn't have a parent applicant folder, we probably reached the root of the drive
        return False

    parent_id = parents[0]
    if parent_id == folder_id:
        return True

    parent = folders_by_id.get(parent_id)
    if parent is None or parent == file:
        # File doesn't have a parent applicant folder, we probably reached the root of the drive
        return False

    return is_file_inside_folder(parent, folder_id, folders_by_id)
