import os
import logging
import sys
from operator import itemgetter

from googleapiclient.http import MediaIoBaseUpload
from ugraddrivesync import get_credentials, get_files_and_folders_for_drive
from googleapiclient.discovery import build
from ugraddrivesync.qualtrics import qualtrics
from .. import COMMON_PARAMS, gapi_request, get_applicant_folders, timeout_checker

LOG = logging.getLogger(__name__)

GOOGLE_SCOPES = [
    'https://www.googleapis.com/auth/drive',
    'https://www.googleapis.com/auth/drive.file',
    'https://www.googleapis.com/auth/drive.appfolder',
]

# Files list page size (this is also the maximum)
FILES_LIST_PAGE_SIZE = 1000

# A query clause for Google Drive API files list call limiting it only to PDF files
PDFS_ONLY_QUERY_CLAUSE = (
    "(mimeType = 'application/pdf')"
)

# A query clause for Google Drive API files list call limiting it only to folders
FOLDERS_ONLY_QUERY_CLAUSE = (
    "(mimeType = 'application/vnd.google-apps.folder')"
)

with open(f'{os.path.dirname(__file__)}/output_template.html', 'r') as f:
    HTML_TEMPLATE = f.read()


def get_applicant_folders_by_ucas(drive_service, qualtrics_settings, has_timed_out):
    """
    Gets a dictionary of applicant folders (from gdrive) indexed by their applicant's UCAS PID
    """

    google_drives = qualtrics_settings['google_drive']
    college_drives = google_drives.get('colleges', [])

    default_drive_id = google_drives['drive_id']
    default_folder_id = google_drives.get('folder_id', default_drive_id)

    college_drives.append({
        'code': 'DEFAULT',
        'description': 'Default college drive',
        'drive_id': default_drive_id,
        'folder_id': default_folder_id
    })

    applicant_folders_by_ucas = {}
    for college_drive in college_drives:
        drive_id = college_drive['drive_id']
        folder_id = college_drive.get('folder_id', drive_id)

        for applicant_folder in get_applicant_folders(
            drive_service.files(),
            drive_id,
            folder_id,
            has_timed_out
        ):
            if 'appProperties' not in applicant_folder:
                LOG.warning(f"unrecognised folder: {applicant_folder['id']} "
                            f"({applicant_folder['webViewLink']})")
                continue

            ucas_number = applicant_folder['appProperties']['ucas_number']
            applicant_folders_by_ucas.setdefault(ucas_number, [])
            applicant_folders_by_ucas[ucas_number].append(applicant_folder)

    return applicant_folders_by_ucas


def get_response_files_by_id(drive_service, qualtrics_settings, has_timed_out):
    """
    Gets a dictionary of Qualtrics survey responses on Google Drive indexed by their response ID.
    The Google Drive file metadata appProperties.surveyResponseId is used to detect response
    files. Mimetype is used to reduce the number of files returned.
    """
    google_drive_settings = qualtrics_settings['google_drive']

    # Create a list of all the searchable drives (all college drives and the default drive)
    default_drive_id = google_drive_settings.get('drive_id')
    searchable_drives_and_folders = [default_drive_id] + [
        record['drive_id'] for record in google_drive_settings.get('colleges', [])
    ]

    response_files_by_id = {}
    for search_drive_id in searchable_drives_and_folders:
        LOG.debug(f'Retrieving CAPO info for drive {search_drive_id}')

        drive_files_and_folders = get_files_and_folders_for_drive(
            drive_service.files(), search_drive_id, PDFS_ONLY_QUERY_CLAUSE, has_timed_out
        )

        for _, item in drive_files_and_folders.items():
            response_id = item.get('appProperties', {}).get('surveyResponseId', None)
            if response_id is not None:
                response_files_by_id[response_id] = item

    return response_files_by_id


def get_mismatched_response_folders_list(
    drive_service, qualtrics_settings, mismatched_folder_id, has_timed_out
):
    """
    Get a list of all folders inside the mismatched response folder. Each folder should correspond
    to an applicant's response (but we lack the meta-data to search for such folders).
    """
    google_drive_settings = qualtrics_settings['google_drive']
    default_drive_id = google_drive_settings.get('drive_id')

    mismatched_response_folders = []
    LOG.debug(
        f'Retrieving mismatched response folder info from default drive {default_drive_id}'
    )
    drive_files_and_folders = get_files_and_folders_for_drive(
        drive_service.files(), default_drive_id,
        f"('{mismatched_folder_id}' in parents) and {FOLDERS_ONLY_QUERY_CLAUSE}",
        has_timed_out
    )

    for _, item in drive_files_and_folders.items():
        mismatched_response_folders.append(item)

    return mismatched_response_folders


def upload_file_to_drive(drive_service, file, file_metadata, mimetype, has_timed_out):
    """
    Uploads files into google drive
    """
    media = MediaIoBaseUpload(file, mimetype=mimetype)
    return gapi_request(lambda: drive_service.files().create(
        body=file_metadata,
        media_body=media,
        fields='id',
        supportsAllDrives=True
    ), has_timed_out).execute()


def create_drive_item(drive_service, metadata, has_timed_out):
    """
    Creates file/folders in google drive.

    The metadata argument contains information about how google should interpret the item:
    {
        'name': 'some_folder',
        'parents': [folder_id],
        'permissionIds': [...],
        'mimeType': 'application/vnd.google-apps.folder'
    }
    """
    return gapi_request(lambda: drive_service.files().create(
        body=metadata,
        fields='id',
        supportsAllDrives=True
    ).execute(), has_timed_out).get('id')


def move_drive_file(drive_service, file_item, destination_folder_id, has_timed_out):
    """
    Moves file/folder from `source_folder` to `destination_folder`
    """
    gapi_request(lambda: drive_service.files().update(
        fileId=file_item['id'],
        addParents=destination_folder_id,
        removeParents=','.join(file_item['parents']),
        fields='id, parents',
        **COMMON_PARAMS
    ).execute(), has_timed_out)


def question_id_from_q_name(survey_questions, question_name):
    # Searches for a question with the matching name, returning the id
    return next(
        question_id for (question_id, question) in
        list(survey_questions.items()) if question['DataExportTag'] == question_name
    )


def sync_with_google_drive(settings, *, dry_run=False):
    """
    Creates a pdf for every response to a qualtrics survey in the appropriate applicant's folder
    """

    drive_credentials = get_credentials(settings, scopes=GOOGLE_SCOPES)
    drive_service = build('drive', 'v3', credentials=drive_credentials)

    qualtrics_settings = settings.get('qualtrics')
    if qualtrics_settings is None:
        LOG.error('The qualtrics setting must be provided.')
        sys.exit(1)

    # Start the main sync task
    with timeout_checker(qualtrics_settings) as has_timed_out:

        applicant_folders_by_ucas = get_applicant_folders_by_ucas(
            drive_service, qualtrics_settings, has_timed_out
        )
        response_files_by_id = get_response_files_by_id(
            drive_service, qualtrics_settings, has_timed_out
        )

        access_token = qualtrics.get_access_token(qualtrics_settings)
        qualtrics_surveys = qualtrics_settings['surveys']

        # For each survey, match each response against a file on Google Drive. If no match is found
        # then generate a PDF from the response and upload to Google Drive.
        for qualtrics_survey_settings in qualtrics_surveys:
            survey_id = qualtrics_survey_settings['id']
            LOG.info(f"Processing responses for survey {survey_id}")

            mismatched_folder_id = qualtrics_survey_settings['mismatched_google_folder_id']
            ucas_id_question_name = qualtrics_survey_settings['ucas_id_question_name']
            upload_filename = qualtrics_survey_settings['filename']
            folder_name_question_names = qualtrics_survey_settings.get('folder_name_questions', [])

            survey = qualtrics.get_survey(
                qualtrics_settings, access_token, survey_id, has_timed_out
            )
            responses = qualtrics.get_survey_responses(
                qualtrics_settings, access_token, survey_id, has_timed_out
            )

            survey_questions = survey['Questions']
            ucas_id_question_id = question_id_from_q_name(survey_questions, ucas_id_question_name)
            folder_name_question_ids = [
                question_id_from_q_name(survey_questions, question_name)
                for question_name in folder_name_question_names
            ]

            files_uploaded = 0
            files_skipped = 0
            files_mismatched = 0

            for response in responses:
                response_values = response['values']

                # Skip incomplete responses
                if response_values.get('finished', 0) != 1:
                    continue

                response_id = response['responseId']

                # Skip already processed responses
                if response_id in response_files_by_id:
                    files_skipped += 1
                    continue

                ucas_id = response_values.get(f'{ucas_id_question_id}_TEXT', '')
                ucas_id = ucas_id.replace(" ", "").replace("-", "")

                applicant_folders = applicant_folders_by_ucas.get(ucas_id)
                if applicant_folders is not None:
                    # There might be multiple folders for the same applicant
                    # (if an error hasoccured), we just use the first one.
                    # This will be tidied up by the other script
                    applicant_folder = applicant_folders[0]
                    folder_id = applicant_folder['id']
                else:
                    files_mismatched += 1
                    LOG.info(f"Response {response['responseId']} to survey {survey_id} did not"
                             " contain a valid UCAS_ID, placing in mismatched folder")
                    applicant_folder = gapi_request(lambda: drive_service.files().get(
                        fileId=mismatched_folder_id,
                        supportsAllDrives=True,
                        fields="permissionIds"
                    ).execute(), has_timed_out)
                    folder_id = mismatched_folder_id

                files = qualtrics.get_response_files(
                    qualtrics_settings,
                    access_token,
                    survey,
                    response,
                    HTML_TEMPLATE,
                    f'{upload_filename}.pdf',
                    has_timed_out
                )

                if not dry_run:
                    folder_name_extensions = [
                        response_values[f'{q}_TEXT']
                        for q in folder_name_question_ids
                        if response_values.get(f'{q}_TEXT', '').strip()
                    ]
                    if len(folder_name_extensions) == 0:
                        folder_name_extensions = [response_id]
                    folder_name = f'{upload_filename} - {" - ".join(folder_name_extensions)}'
                    response_folder_id = create_drive_item(drive_service, {
                        'name': folder_name,
                        'parents': [folder_id],
                        'mimeType': 'application/vnd.google-apps.folder'
                    }, has_timed_out)
                else:
                    LOG.info(f"Would create folder for response: {response_id}")

                for file in files:
                    filename, content, mime = itemgetter('filename', 'content', 'mime')(file)
                    files_uploaded += 1
                    if not dry_run:
                        upload_file_to_drive(drive_service, content, {
                            'name': filename,
                            'parents': [response_folder_id],
                            'appProperties': {
                                'surveyResponseId': response_id
                            }
                        }, mime, has_timed_out)
                    else:
                        LOG.info(f"Would upload file for response: {response_id}")

            # Check whether any response files in the mismatched response folder can now be matched
            # against applicant folders that have been recently created. Process the folders
            # containing the response files to do this (making the assumption that all folders in
            # the mismatched response folder contain corresponding response files).
            mismatched_response_folders = get_mismatched_response_folders_list(
                drive_service, qualtrics_settings, mismatched_folder_id, has_timed_out
            )
            responses_moved = 0
            for folder in mismatched_response_folders:
                folder_name = folder.get('name', '')
                parsed_folder_name = folder_name.split('-')
                if len(parsed_folder_name) <= 0:
                    continue
                possible_ucas_pid = parsed_folder_name[-1].strip()
                if possible_ucas_pid in applicant_folders_by_ucas:
                    applicant_folders = applicant_folders_by_ucas[possible_ucas_pid]
                    if not dry_run:
                        move_drive_file(
                            drive_service, folder, applicant_folders[0]['id'], has_timed_out
                        )
                    else:
                        LOG.info(
                            "Would move mismatched response folder to applicant folder with ID: " +
                            f"{applicant_folders[0]['id']}"
                        )
                    responses_moved += 1

            LOG.info(f"Finished processing qualtrics responses for survey {survey_id}")
            LOG.info(f"Responses created: {files_uploaded}")
            LOG.info(f"Responses without matching UCAS PID: {files_mismatched}")
            LOG.info(f"Responses skipped (already exist): {files_skipped}")
            LOG.info(
                "Responses moved (previously mismatched responses matched against applicants): " +
                f"{responses_moved}"
            )
            LOG.info(f"Responses total (including skipped): {len(responses)}")
