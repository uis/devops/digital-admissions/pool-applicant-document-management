"""
Accesses the qualtrics API to pull survey responses and their associated files.

Qualtrics use oauth to secure their API so we use a clientId and secret to retrieve an access token
which can be used for later requests.

The general process for interacting with the qualtrics api are:
- Request an API key (using the clientId and secret)
- Ask the qualtrics api to export survey responses
- Poll the API for the status of that export
- Retrieve the export
"""

import base64
import html
import io
import time
import requests
import pdfkit
import logging

from ugraddrivesync import api_request

LOG = logging.getLogger(__name__)

# The OAuth scopes required to be able to see a survey and retrieve its responses
QUALTRICS_SCOPES = [
    "read:surveys",
    "read:survey_responses"
]


def get_access_token(qualtrics_settings):
    """
    Retrieve the access token to be used for all other requests to the qualtrics api
    """
    qualtrics_server = qualtrics_settings['url']
    client_id = qualtrics_settings['clientId']
    secret = qualtrics_settings['secret']

    response = requests.post(
        f"{qualtrics_server}/oauth2/token",
        data={
            "grant_type": "client_credentials",
            "scope": " ".join(QUALTRICS_SCOPES)
        },
        auth=(client_id, secret)
    )
    response.raise_for_status()
    data = response.json()
    return data['access_token']


def export_survey_responses(qualtrics_settings, access_token, survey_id, has_timed_out):
    """
    Starts a qualtrics survey data export.
    This returns a progress id that can be used to poll the status of the export.
    """
    qualtrics_server = qualtrics_settings['url']

    data = qualtrics_request(lambda: requests_post(
        f"{qualtrics_server}/API/v3/surveys/{survey_id}/export-responses",
        json={
            "format": "json",
            "compress": False
        },
        headers={
            "Authorization": f"Bearer {access_token}"
        }
    ), has_timed_out).json()
    result = data['result']
    status = result['status']
    if status not in ['inProgress', 'complete']:
        raise Exception(f'Could not create qualtrics data export: {result}')
    return result['progressId']


def poll_for_export_complete(
    qualtrics_settings,
    access_token,
    survey_id,
    export_id,
    has_timed_out,
    timeout_seconds=300,
    poll_every_seconds=10
):
    """
    Polls the qualtrics api repeatedly to check the status of a survey data export.
    When the export is completed a file id is returned that can be used to retrieve the export.
    """
    qualtrics_server = qualtrics_settings['url']
    start_time = time.time()
    while time.time() - start_time < timeout_seconds:
        data = qualtrics_request(lambda: requests_get(
            f"{qualtrics_server}/API/v3/surveys/{survey_id}/export-responses/{export_id}",
            headers={
                "Authorization": f"Bearer {access_token}"
            }
        ), has_timed_out).json()
        result = data['result']
        status = result['status']
        if status not in ['inProgress', 'complete']:
            raise Exception(f'Qualtrics data export failed: {result}')
        if status == 'complete':
            return result['fileId']
        time.sleep(poll_every_seconds)
    else:
        raise Exception(f'Export did not complete in in {timeout_seconds} seconds')


def get_export(qualtrics_settings, access_token, survey_id, file_id, has_timed_out):
    """
    Gets the survey response export that has previously been requested
    """
    qualtrics_server = qualtrics_settings['url']
    return qualtrics_request(lambda: requests_get(
        f"{qualtrics_server}/API/v3/surveys/{survey_id}/export-responses/{file_id}/file",
        headers={
            "Authorization": f"Bearer {access_token}"
        }
    ), has_timed_out).json()


def get_survey_responses(qualtrics_settings, access_token, survey_id, has_timed_out):
    """
    Gets a list of qualtrics survey responses
    """
    export_id = export_survey_responses(qualtrics_settings, access_token, survey_id, has_timed_out)
    file_id = poll_for_export_complete(
        qualtrics_settings, access_token, survey_id, export_id, has_timed_out
    )
    result = get_export(qualtrics_settings, access_token, survey_id, file_id, has_timed_out)
    return result['responses']


def get_uploaded_file(settings, access_token, survey_id, response_id, file_id, has_timed_out):
    """
    Gets a file uploaded as part of a qualtrics survey response
    """
    qualtrics_server = settings['url']
    return qualtrics_request(lambda: requests_get(
        f"{qualtrics_server}/API/v3/"
        f"surveys/{survey_id}/"
        f"responses/{response_id}/"
        f"uploaded-files/{file_id}",
        headers={
            "Authorization": f"Bearer {access_token}"
        }
    ), has_timed_out).content


def get_survey(qualtrics_settings, access_token, survey_id, has_timed_out):
    """
    Gets the qualtrics survey definition
    """
    qualtrics_server = qualtrics_settings['url']
    data = qualtrics_request(lambda: requests_get(
        f"{qualtrics_server}/API/v3/survey-definitions/{survey_id}",
        headers={
            "Authorization": f"Bearer {access_token}"
        }
    ), has_timed_out).json()
    return data['result']


def get_response_files(
    qualtrics_settings,
    access_token,
    survey,
    response,
    html_template,
    response_file_name,
    has_timed_out
):
    """
    Creates a pdf for the qualtrics survey response and gets all files uploaded as part of that
    response
    """
    files = []

    answers = response['values']
    labels = response['labels']

    questions_and_answers = []

    def get_safe_answer(question_id, question_schema):
        question_type = question_schema['QuestionType']

        # Decide how we'll display each type of question
        if question_type == 'DB':  # Text block (not an input)
            safe_answer = None
        elif question_type == 'TE':  # Text Entry
            if not answers.get(f'{question_id}_TEXT'):
                safe_answer = "Not provided"
            else:
                safe_answer = html.escape(answers[f'{question_id}_TEXT'])
        elif question_type == 'MC':  # Multiple choice
            if not labels.get(question_id):
                safe_answer = "Not provided"
            else:
                question_labels = labels[question_id]
                if isinstance(question_labels, list):
                    labels_combined = ''.join(
                        f'<tr><td>{html.escape(label)}</td></tr>' for label in question_labels
                    )
                    safe_answer = f'<table>{labels_combined}</table>'
                else:
                    safe_answer = html.escape(question_labels)
        elif question_type == 'Draw':  # Signature
            if not answers.get(f'{question_id}_FILE_ID'):
                safe_answer = "Not provided"
            else:
                file_id = answers[f'{question_id}_FILE_ID']
                file = get_uploaded_file(
                    qualtrics_settings,
                    access_token,
                    survey['SurveyID'],
                    response['responseId'],
                    file_id,
                    has_timed_out
                )
                base64_file = base64.b64encode(file)
                file_mime = answers[f'{question_id}_FILE_TYPE']
                safe_answer = f'<img src="data:{file_mime};base64,{base64_file.decode("utf-8")}"/>'
        elif question_type == 'FileUpload':
            if not answers.get(f'{question_id}_FILE_ID'):
                safe_answer = "Not provided"
            else:
                file_id = answers[f'{question_id}_FILE_ID']
                file = get_uploaded_file(
                    qualtrics_settings,
                    access_token,
                    survey['SurveyID'],
                    response['responseId'],
                    file_id,
                    has_timed_out
                )
                file_name = answers[f'{question_id}_FILE_NAME']
                files.append({
                    "filename": file_name,
                    "content": io.BytesIO(file),
                    "mime": answers[f'{question_id}_FILE_TYPE']
                })
                safe_answer = f'Attachment: {html.escape(file_name)}'
        elif question_type == 'Matrix':
            safe_answer = '<table>\n'
            for choice_id in question_schema['ChoiceOrder']:
                choice = question_schema['Choices'][str(choice_id)]
                safe_answer += '<tr>'
                safe_answer += f'<td>{choice["Display"]}</td>'

                if question_schema['SubSelector'] == 'DL':  # Dropdown list
                    question_answer = answers.get(f'{question_id}_{choice_id}')
                    if question_answer:
                        dropdown_answers = question_schema['Answers'][str(choice_id)]
                        dropdown_answer = dropdown_answers[str(question_answer)]
                        safe_answer += f'<td>{dropdown_answer["Display"]}</td>'
                    else:
                        safe_answer += '<td>Not provided</td>'
                else:
                    raise Exception('Unknown Matrix question sub-type: '
                                    f'{question_schema["SubSelector"]}')
                safe_answer += '</tr>\n'
            safe_answer += '</table>'
        else:
            raise Exception(f'Unknown question type: {question_type}')

        return safe_answer

    # A somewhat naive attempt to list the questions in the order they appear in the
    # survey. It doesn't handle branching question flows, and the code only handles a subset
    # of the types of questions that Qualtrics supports.
    for flow_item in survey['SurveyFlow']['Flow']:
        # We currently only handle Block and Standard types (which appear to be the same), other
        # flow item types include 'Branch'. Branches can contain other questions but are so far
        # only used to exit the survey.
        if flow_item['Type'] in ['Block', 'Standard']:
            # Each flow contains a list of blocks, some of which are questions.
            for block_element in survey['Blocks'][flow_item['ID']]['BlockElements']:
                if block_element['Type'] == 'Question':
                    question_id = block_element['QuestionID']
                    question_schema = survey['Questions'][question_id]
                    questions_and_answers.append({
                        'question': question_schema['QuestionText'],
                        'safeAnswer': get_safe_answer(question_id, question_schema)
                    })

    html_snippet = '<table>\n'
    for questions_and_answer in questions_and_answers:
        html_snippet += f'<tr><td>{questions_and_answer["question"]}</td></tr>\n'
        if questions_and_answer["safeAnswer"]:
            html_snippet += '<tr>'
            html_snippet += f'<td><b>{questions_and_answer["safeAnswer"]}</b></td>'
            html_snippet += '</tr>\n'
        html_snippet += '<tr><td><hr/></td></tr>\n'
    html_snippet += '</table>'

    output = html_template.replace('<!-- MARKER -->', html_snippet)
    response_pdf = pdfkit.from_string(output, output_path=False, options={
        "quiet": "",
        "page-size": "A4",
        "enable-forms": ""
    })

    files.append({
        "filename": response_file_name,
        "content": io.BytesIO(response_pdf),
        "mime": "application/pdf"
    })

    return files


def requests_get(*args, **kwargs):
    """Simplifies call to requests.get()"""
    response = requests.get(*args, **kwargs)
    response.raise_for_status()
    return response


def requests_post(*args, **kwargs):
    """Simplifies call to requests.post()"""
    response = requests.post(*args, **kwargs)
    response.raise_for_status()
    return response


def qualtrics_request(request_fn, has_timed_out):
    """Specialisation of `api_request()` for making calls with the `requests` client."""
    return api_request(
        request_fn, has_timed_out, requests.exceptions.HTTPError, lambda e: e.response.status_code
    )
