import logging
import logging.config
from functools import reduce
from os import sys
from time import sleep, time
from contextlib import contextmanager

import deepmerge
import geddit
import google.auth
import google.oauth2
import requests
import yaml
from googleapiclient.errors import HttpError

from .urls import merge_url_query

LOG = logging.getLogger(__name__)

# Default timeout for a process (in seconds), beyond which a graceful exit will be performed.
DEFAULT_PROCESS_TIMEOUT = 14 * 60

# Parameters common to all Google Drive API calls
COMMON_PARAMS = {
    # Access shared drives (not just My Drive)
    'supportsAllDrives': True,
}

# A query clause Google Drive API files list call limiting it only to folders.
FOLDERS_ONLY_QUERY_CLAUSE = "(mimeType = 'application/vnd.google-apps.folder')"
# Files list page size (this is also the maximum)
FILES_LIST_PAGE_SIZE = 1000


# Exception that can be raised when timeout_checker reports a process timeout
class ProcessTimeout(Exception):
    pass


def load_settings(urls):
    settings = {}
    for url in urls:
        LOG.info('Loading settings from %s', url)
        settings = deepmerge.always_merger.merge(settings, yaml.safe_load(geddit.geddit(url)))
    return settings


def get_credentials(settings, *, scopes=None):
    """
    Get Google API credentials for the passed settings dict

    """
    google_settings = settings.get('google', {})
    credentials_path = google_settings.get('credentials_path', None)

    # If we have no credentials path, use the default credentials. Otherwise load the service
    # account credentials from the path.
    if credentials_path is None:
        credentials, _ = google.auth.default(scopes=scopes)
    else:
        credentials = (
            google.oauth2.service_account.Credentials
            .from_service_account_file(credentials_path)
            .with_scopes(scopes)
        )

    return credentials


def fetch_json(url, *, query=None, headers=None):
    """
    Fetch a JSON object and return as a dictionary.
    """
    query = query if query is not None else {}
    headers = headers if headers is not None else {}

    # Merge the specified query into the URL respecting any existing query params
    url = merge_url_query(url, query)

    LOG.info('Fetching %s', url)
    response = requests.get(url, headers=headers)
    response.raise_for_status()
    return response.json()


def post_json(url, data, *, headers=None, dry_run=True):
    """
    Post data to an API endpoint.
    """
    if dry_run:
        LOG.info(f'Would post to {url}: {data}')
    else:
        headers = headers if headers is not None else {}
        response = requests.post(url, json=data, headers=headers)
        if response.ok:
            LOG.info(f'Posted to {url}: {data}')
            return response.json()

        LOG.error(f'Failed to post to {url}: {data}')
        LOG.error(response.text)


def fetch_applications(url, *, query=None, headers=None):
    """
    Fetch the pool applicants CSV file and generate rows as a `dict` keyed by slugs of the
    CSV headers.
    """
    query = query if query is not None else {}
    headers = headers if headers is not None else {}

    # Merge the specified query into the URL respecting any existing query params
    url = merge_url_query(url, query)

    while url is not None and url != '':
        LOG.info('Fetching %s', url)
        response = requests.get(url, headers=headers)
        response.raise_for_status()
        data = response.json()
        results = data['results']

        LOG.info('Processing %s rows', len(results))
        for application in results:
            yield application

        # Use the "next" URL for the next page
        url = data.get('next')


def get_required_setting(settings, dottedkey):
    """
    Get's a setting using a dotted key reference. Exits with an error if the setting isn't found.
    """
    setting = get(settings, dottedkey)

    if setting is None:
        LOG.error('The %s setting must be provided.', dottedkey)
        sys.exit(1)

    return setting


def get(_dict, dottedkey, default=None):
    """
    Get the value from a nested dict using a dotted key reference (e.g. key1.key2.key3)

    >>> _dict = {'a':{'b':'c','d':{'e':'f'}},'g':'h'}
    >>> get(_dict, 'g')
    'h'
    >>> get(_dict, 'a.b')
    'c'
    >>> get(_dict, 'a.d.e')
    'f'
    >>> get(_dict, 'x', 'missing')
    'missing'
    >>> get(_dict, 'a.x', 'missing')
    'missing'
    >>> get(_dict, 'a.d.x', 'missing')
    'missing'
    >>> get(_dict, 'a.x.y', 'missing')
    'missing'
    """
    return reduce(
        lambda d, k: d.get(k, default) if isinstance(d, dict) else default,
        dottedkey.split('.'),
        _dict
    )


@contextmanager
def timeout_checker(settings):
    """
    A context manager that starts a timer and yields a closure that checks if the defined timeout
    has been passed.
    """
    process_timeout = settings.get('process_timeout', DEFAULT_PROCESS_TIMEOUT)
    end_time = time() + int(process_timeout)

    def has_timed_out():
        if time() >= end_time:
            LOG.warning(f'Timeout reached ({process_timeout}s) -- exiting gracefully')
            return True
        return False

    yield has_timed_out


def get_applicant_folders(files_service, drive_id, folder_id, has_timed_out):
    """
    Retrieve all of the applicant folders in a drive folder
    """
    page_token = None
    while True:
        LOG.debug('Retrieving page of applicant folders')
        response = gapi_request(lambda: files_service.list(
            q=(
                f"(trashed = false) and ('{folder_id}' in parents) and " +
                FOLDERS_ONLY_QUERY_CLAUSE
            ),
            fields='nextPageToken, '
                   'files('
                   'id, name, parents, appProperties, permissionIds, description, webViewLink'
                   ')',
            pageToken=page_token,
            corpora='drive',
            driveId=drive_id,
            includeItemsFromAllDrives=True,
            pageSize=FILES_LIST_PAGE_SIZE,
            **COMMON_PARAMS
        ).execute(), has_timed_out)
        for folder in response.get('files', []):
            yield folder
        page_token = response.get('nextPageToken', None)
        if page_token is None:
            break


def get_files_and_folders_for_drive(files_service, drive_id, query, has_timed_out):
    """
    Gets a dictionary of files and folders (that match query expression) from google drive for a
    particular drive, keyed by id. The term `trashed = false and ` is always added to the start of
    the query expression to filter out deleted items.

    Query term specification: https://developers.google.com/drive/api/v3/search-files
    """
    files_by_id = {}
    page_token = None
    while True:
        LOG.debug('Retrieving page of files/folders')
        response = gapi_request(lambda: files_service.list(
            q=f'(trashed = false) and {query}',
            fields='nextPageToken, '
                   'files(id, name, description, appProperties, parents)',
            pageToken=page_token,
            corpora='drive',
            driveId=drive_id,
            includeItemsFromAllDrives=True,
            pageSize=FILES_LIST_PAGE_SIZE,
            **COMMON_PARAMS
        ).execute(), has_timed_out)
        for file in response.get('files', []):
            files_by_id[file['id']] = file
        page_token = response.get('nextPageToken', None)
        if page_token is None:
            break
    return files_by_id


def api_request(request_fn, has_timed_out, error_class, status_code, retries=5):
    """
    Executes the passed lambda function, which should make a request to an API. If certain
    exceptions occurs (e.g. due to quota being exceeded) then retry with exponential back-off.
    `error_class` & `status_code` are used to catch and read the client specific exception.
    `has_timed_out` is checked (and actioned) at the start and during retries.
    """
    if has_timed_out():
        raise ProcessTimeout()
    retry_delay = 5
    while True:
        try:
            return request_fn()
        except error_class as err:
            # If retries exceeded or error code is other than `retry_errors` then re-raise
            # the error, otherwise delay and retry
            LOG.info(f'API error {status_code(err)}.')
            if status_code(err) not in [403, 429, 500, 503] or retries <= 0:
                LOG.info("Not retrying.")
                raise
            LOG.info(f'Retrying after {retry_delay}s...')
            for _ in range(retry_delay):
                sleep(1)
                if has_timed_out():
                    raise ProcessTimeout()
            retries -= 1
            retry_delay *= 2


def gapi_request(request_fn, has_timed_out):
    """Specialisation of `api_request()` for making Google API calls. """
    return api_request(request_fn, has_timed_out, HttpError, lambda e: e.resp.status)
