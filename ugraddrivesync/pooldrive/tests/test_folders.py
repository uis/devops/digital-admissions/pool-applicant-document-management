from ugraddrivesync.pooldrive.folders import pop_best_folder


def test_pop_best_folder__pops_first_if_only():
    folder_0 = {
        "id": "folder0",
        "parents": [],
        "appProperties": {"ucas_number": "aa"}
    }

    folders = [folder_0]

    assert pop_best_folder(folders, 'anything') == folder_0
    assert len(folders) == 0


def test_pop_best_folder__pops_folder_with_correct_parent():
    folder_0 = {
        "id": "folder0",
        "parents": ["incorrect"],
        "appProperties": {"ucas_number": "aa"}
    }
    folder_1 = {
        "id": "folder1",
        "parents": ["correct"],
        "appProperties": {"ucas_number": "bb"}
    }
    folder_2 = {
        "id": "folder2",
        "parents": ["incorrect2"],
        "appProperties": {"ucas_number": "cc"}
    }

    folders = [folder_0, folder_1, folder_2]

    assert pop_best_folder(folders, 'correct') == folder_1
    assert folders == [folder_0, folder_2]


def test_pop_best_folder__pops_last_if_no_matching_parent():
    folder_0 = {
        "id": "folder0",
        "parents": ["incorrect"],
        "appProperties": {"ucas_number": "aa"}
    }
    folder_1 = {
        "id": "folder1",
        "parents": ["incorrect2"],
        "appProperties": {"ucas_number": "bb"}
    }

    folders = [folder_0, folder_1]

    assert pop_best_folder(folders, 'correct') == folder_1
    assert folders == [folder_0]
