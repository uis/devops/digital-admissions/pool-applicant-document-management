from ugraddrivesync.pooldrive.validator import check_ucas_pid_like_values, contains_ucas_pid, \
    is_file_inside_folder, FILE_NAME_ERROR_MSGS


# Define error codes as global constants (could create dynamically but then we'd get PEP8 error
# F821)
PID_PROBABLE_MISTYPE = FILE_NAME_ERROR_MSGS['PID_PROBABLE_MISTYPE']
PID_MISMATCH_FOLDER = FILE_NAME_ERROR_MSGS['PID_MISMATCH_FOLDER']


def test_folder_name_contains_ucas_pid():
    """Check that the applicant folder name contains the UCAS PID"""

    test_cases = [
        ("123", "123", True),
        ("123.zip", "123", True),
        ("abc.123.zip", "123", True),
        ("abc-123", "123", True),
        ("abc-4567890-abc", "4567890", True),
        ("abc_4567890_abc", "4567890", True),
        ("abc-4567890_abc", "4567890", True),
        ("abc 4567890 abc", "4567890", True),
        ("abc_4567890 abc", "4567890", True),
        ("abc(4567890)abc", "4567890", True),
        ("abc", "123", False),
        ("", "123", False),
        ("abc123abc", "123", False),  # Must be separated from any other characters/numbers
        ("456123789", "123", False),  # Must be separated from any other characters/numbers
        ("456-123-789", "123", True),
    ]

    for filename, ucas_pid, expected_result in test_cases:
        result = contains_ucas_pid(filename, ucas_pid)
        assert result == expected_result, \
            f'Folder name: {filename} -- expected: {expected_result}, got: {result}'


def test_file_name_contains_ucas_pid():
    """
    Check that the file name contains the UCAS PID

    This is a broader check than for the applicant folder name. Attempts are made to identify
    possible PIDs, and errors flagged if they seem incorrect.
    """
    test_cases = [
        ("1234567890", "1234567890", (True, None)),
        ("123-456-7890", "1234567890", (True, None)),
        ("1234567890.zip", "1234567890", (True, None)),
        ("abc.1234567890.zip", "1234567890", (True, None)),
        ("abc-1234567890", "1234567890", (True, None)),
        ("abc-1234567890-abc", "1234567890", (True, None)),
        ("abc-12-34-56-78-90-abc", "1234567890", (True, None)),
        ("abc_1234567890_abc", "1234567890", (True, None)),
        ("abc-1234567890_abc", "1234567890", (True, None)),
        ("abc 1234567890 abc", "1234567890", (True, None)),
        ("abc_1234567890 abc", "1234567890", (True, None)),
        ("abc(1234567890)abc", "1234567890", (True, None)),
        # Nothing resembling a PID
        ("abc", "1234567890", (True, None)),
        # Nothing resembling a PID
        ("", "1234567890", (True, None)),
        ("abc1234567890abc", "1234567890", (True, None)),
        # Must be separated from any other numbers, but sequences of >11 digits are ignored
        ("61234567890", "1234567890", (False, PID_PROBABLE_MISTYPE)),
        ("12345678907", "1234567890", (False, PID_PROBABLE_MISTYPE)),
        ("4561234567890789", "1234567890", (True, None)),
        ("456-1234567890-789", "1234567890", (True, None)),
        ("1234567790", "1234567890", (False, PID_MISMATCH_FOLDER)),
        ("1234567890_1234567890", "1234567890", (True, None)),
        ("1234567890_1234567790", "1234567890", (False, PID_MISMATCH_FOLDER)),
        # Sequences of <9 digits are ignored
        ("12346789", "1234567890", (True, None)),
        ("123467890", "1234567890", (False, PID_PROBABLE_MISTYPE)),
        ("298763548", "1234567890", (False, PID_PROBABLE_MISTYPE)),
        ("1234567890_210012345_abc", "1234567890", (True, None)),
    ]

    for filename, ucas_pid, expected_result in test_cases:
        result = check_ucas_pid_like_values(filename, ucas_pid)
        assert result == expected_result, \
            f'File name: {filename} -- expected: {expected_result}, got: {result}'


def test_is_file_inside_folder__basic():
    """If file is inside folder then is_file_inside_folder returns true"""
    folder_id = 'folder_id'
    file = {
        'id': 'file_id',
        'parents': [folder_id]
    }
    files_by_id = {
        folder_id: {'id': folder_id},
        file['id']: file
    }

    assert is_file_inside_folder(file, folder_id, files_by_id)


def test_is_file_inside_folder__file_is_folder():
    """If file is folder then is_file_inside_folder returns true"""
    folder = {
        'id': 'folder_id',
        'parents': ['anything']
    }
    files_by_id = {
        folder['id']: folder
    }

    assert is_file_inside_folder(folder, folder['id'], files_by_id)


def test_is_file_inside_folder__nesting():
    """If file is nested inside folder then is_file_inside_folder returns true"""
    folder0 = {
        'id': 'folder0',
        'parents': ['anything']
    }
    folder1 = {
        'id': 'folder1',
        'parents': [folder0['id']]
    }
    folder2 = {
        'id': 'folder2',
        'parents': [folder1['id']]
    }
    file = {
        'id': 'file_id',
        'parents': [folder2['id']]
    }
    files_by_id = {
        folder0['id']: folder0,
        folder1['id']: folder1,
        folder2['id']: folder2,
        file['id']: file,
    }

    assert is_file_inside_folder(file, folder0['id'], files_by_id)


def test_is_file_inside_folder__not_in_folder():
    """If file is nested inside folder then is_file_inside_folder returns true"""
    folder0 = {
        'id': 'folder0',
        'parents': ['anything']
    }
    folder1 = {
        'id': 'folder1',
        'parents': ['anything']
    }
    file = {
        'id': 'file_id',
        'parents': [folder1['id']]
    }
    files_by_id = {
        folder0['id']: folder0,
        folder1['id']: folder1,
        file['id']: file,
    }

    assert not is_file_inside_folder(file, folder0['id'], files_by_id)
