from ugraddrivesync.camsis import create_sync, create_insert, get_field_names, match_filter


def test_create_sync_not_target():
    """no output if the field is not a target of synchronisation"""

    smi_app = {'comments': 'shows promise'}
    camsis_app = {'comments': 'not great'}
    sync = create_sync(camsis_app, smi_app)
    assert sync == {}


def test_create_sync_matching():
    """no output if the fields match"""
    smi_app = {
        'id': '10536824',
        'collegePreference': 'MUR',
        'subject': 'NSTX',
        'admitYear': 2020,
        'camsisStatus': 'APPLICANT',
        'poolType': 'SUMMER',
    }
    camsis_app = {**smi_app}
    sync = create_sync(camsis_app, smi_app)
    assert sync == {}


def test_create_sync_camsisStatus():
    """always sync if camsisStatus differs"""
    smi_app = {'camsisStatus': 'APPLICANT'}
    camsis_app = {'camsisStatus': 'CANCELLED'}
    sync = create_sync(camsis_app, smi_app)
    assert sync == camsis_app


def test_create_sync_poolType():
    """always sync if poolType differs"""
    smi_app = {'poolType': None}
    camsis_app = {'poolType': 'SUMMER'}
    sync = create_sync(camsis_app, smi_app)
    assert sync == camsis_app


def test_create_sync_poolStatus():
    """always sync if poolStatus differs"""
    smi_app = {'poolStatus': None}
    camsis_app = {'poolStatus': 'NOTAG'}
    sync = create_sync(camsis_app, smi_app)
    assert sync == camsis_app


def test_create_sync_subject():
    """always sync if subject differs"""
    smi_app = {'subject': 'NSTX'}
    camsis_app = {'subject': 'LWTX'}
    sync = create_sync(camsis_app, smi_app)
    assert sync == {'subject': 'LWTX'}


def test_create_sync_admitYear():
    """always sync if admitYear differs"""
    smi_app = {'admitYear': 2020}
    camsis_app = {'admitYear': 2021}
    sync = create_sync(camsis_app, smi_app)
    assert sync == {'admitYear': 2021}


def test_create_sync_collegePreference_in_target_pool():
    """nothing returned if collegePreference differs and is in the target pool"""
    smi_app = {
        'poolType': 'SUMMER',
        'collegePreference': 'MUR',
    }
    camsis_app = {
        'poolType': 'SUMMER',
        'collegePreference': 'LC',
    }
    sync = create_sync(camsis_app, smi_app)
    assert sync == {}


def test_create_sync_collegePreference_out_target_pool():
    """sync if collegePreference differs and is outside the target pool"""
    smi_app = {
        'poolType': 'WINTER',
        'collegePreference': 'MUR',
    }
    camsis_app = {
        'poolType': None,
        'collegePreference': 'LC',
    }
    sync = create_sync(camsis_app, smi_app)
    assert sync == {'poolType': None, 'collegePreference': 'LC'}


def test_create_sync_name_changes():
    """sync name changes"""
    smi_app = {
        'candidate': {
            'forenames': 'barry',
            'lastName': 'jones'
        }
    }
    camsis_app = {
        'candidate.forenames': 'larry',
        'candidate.lastName': 'james',
    }
    sync = create_sync(camsis_app, smi_app)
    assert sync == {
        'candidate': {
            'forenames': 'larry',
            'lastName': 'james'
        }
    }


def test_create_sync_name_unchanged():
    """sync name unchanged"""
    smi_app = {
        'candidate': {
            'forenames': 'barry',
            'lastName': 'jones'
        }
    }
    camsis_app = {
        'candidate.forenames': 'barry',
        'candidate.lastName': 'jones',
    }
    sync = create_sync(camsis_app, smi_app)
    assert sync == {}


def test_create_sync_name_changes_missing():
    """sync name changes when name missing"""
    smi_app = {
        'candidate': {
        }
    }
    camsis_app = {
        'candidate.forenames': 'larry',
        'candidate.lastName': 'james',
    }
    sync = create_sync(camsis_app, smi_app)
    assert sync == {
        'candidate': {
            'forenames': 'larry',
            'lastName': 'james'
        }
    }


def test_create_sync_no_ucas_pid_or_usn_changes():
    """don't sync ucasPersonalId or usn changes"""
    smi_app = {
        'candidate': {
            'ucasPersonalId': '54321',
            'camsisUsn': 'xyz'
        }
    }
    camsis_app = {
        'candidate.ucasPersonalId': '12345',
        'camsisUsn': 'zyx'
    }
    sync = create_sync(camsis_app, smi_app)
    assert sync == {}


def test_create_insert():
    """create inserts and defaulted fields corrected"""
    camsis_app = {
        'candidate.ucasPersonalId': '12345',
        'candidate.forenames': 'garry',
        'collegePreference': 'MARS'
    }
    sync = create_insert(camsis_app)
    assert sync == {
        'subjectOptions': [],  # defaulted field
        'collegePreference': 'MARS',
        'candidate': {
            'forenames': 'garry',
            'ucasPersonalId': '12345'
        }
    }


def test_get_field_names():
    assert get_field_names({
        "this": "is",
        "one": {
            "small": "step",
            "for": {
                "a": {},
                "frog": ["..."]
            }
        }
    }) == [
        "this", "one", "one.small", "one.for", "one.for.a", "one.for.frog"
    ]


def test_filter():
    smi_apps = [
        {
            'id': '10536824',
            'subject': 'NSTX',
            'year': 2020,
        },
        {
            'id': '10734675',
            'subject': 'NSTX',
            'year': 2021,
        },
        {
            'id': '10434170',
            'subject': 'LWTX',
            'year': 2022,
        }
    ]

    def compare_apps(apps, filter_list):
        """
        Return the number of applications (apps) that match one or more entries in the filter
        list.
        """
        match_count = 0
        for app in apps:
            if match_filter(filter_list, app):
                match_count += 1
        return match_count

    assert compare_apps(smi_apps, [dict(subject='NSTX')]) == 2
    assert compare_apps(smi_apps, [dict(subject='LWTX')]) == 1
    assert compare_apps(smi_apps, [dict(subject='LWTX'), dict(subject='NSTX')]) == 3
    assert compare_apps(smi_apps, [dict(year=2020)]) == 1
    assert compare_apps(smi_apps, [dict(year=2020, subject='NSTX')]) == 1
    assert compare_apps(smi_apps, [dict(year=2020, subject='LWTX')]) == 0
    assert compare_apps(smi_apps, [dict(year=2022, subject='NSTX')]) == 0
    assert compare_apps(smi_apps, [dict(year=2021), dict(year=2022)]) == 2
