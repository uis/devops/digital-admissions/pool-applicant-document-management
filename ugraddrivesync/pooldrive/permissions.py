import logging
import sys
import time
import random

from googleapiclient.discovery import build

from .. import COMMON_PARAMS, get_credentials, timeout_checker

LOG = logging.getLogger(__name__)

# Google Drive API scopes
SCOPES = [
    'https://www.googleapis.com/auth/drive',
]

# Limit to number of requests sent in batches to the Google API
BATCH_LIMIT = 50

# The maximum duration in seconds that a re-try will be delayed by
MAXIMUM_BACKOFF_DURATION = 32

# Mappings between access level and Google Drive API role
ACCESS_LEVEL_TO_API_ROLE = dict(
    manager='organizer',
    contentmanager='fileOrganizer',
    contributor='writer',
    commenter='commenter',
    viewer='reader',
    none='none'
)
API_ROLE_TO_ACCESS_LEVEL = {
    api_role: access_level for access_level, api_role in ACCESS_LEVEL_TO_API_ROLE.items()
}


def update(settings, *, group_permissions=[], dry_run=False):
    """
    Update user access levels to all college shared drives.

    The "group_permissions" list should contain an entry for each group that
    should have an access level applied to each member of the group across all
    college shared drives. Each entry should be of the form:

    dict(
        group_name=<str>,
        access_level=<str>,
    )

    Groups must be specified in the job specification ("settings"), or must
    be the implicit group "others" which contains each user with access to at
    least one college drive who is not a member of any of the groups in the
    job specification, and is not the account used to authorise this operation.

    Supported access levels (which will be applied to all college drives to
    members of the group) are:

    - manager
    - contentmanager
    - contributor
    - commenter
    - viewer
    - none: revoke access to all college drives from members of this group
    """
    credentials = get_credentials(settings, scopes=SCOPES)
    service = build('drive', 'v3', credentials=credentials)
    service_permissions = service.permissions()

    pooldrive_settings = settings.get('pooldrive')
    if pooldrive_settings is None:
        LOG.error('The pooldrive setting must be provided.')
        sys.exit(1)

    # Configure Google Drive settings
    google_drive_settings = pooldrive_settings.get('google_drive', {})
    per_college_settings = {
        record['code']: record
        for record in google_drive_settings.get('colleges', [])
    }
    per_college_settings['ALL'] = dict(
        code='ALL',
        description='All applicant folders',
        drive_id=google_drive_settings['drive_id']
    )
    groups = {group['name']: group for group in settings.get('groups', {})}

    # Iterate over all colleges in the settings to find all users with access
    # to one or more college drives. Requests are batched together (one request
    # per college drive), and another batch created for each drive that has
    # more permissions to retrieve. Batches continue to be created until all
    # permissions have been retrieved from all drives
    users = set()
    user_perms_by_college = dict()
    batch_current = None
    batch_next = None
    request_by_college = dict()
    need_batch = True  # another batch request is required

    def process_permissions_callback(college_code, response, exception):
        nonlocal users, user_perms_by_college, batch_next, request_by_college, need_batch
        if exception is None:
            # Extract email addresses of the drives users from the response
            perms = response['permissions']
            for perm in perms:
                emailAddress = perm['emailAddress']
                users.add(emailAddress)
                user_perms_by_college[college_code][emailAddress] = perm

            # Add request for next set of results to the next batch (if there
            # are more results to fetch)
            request = request_by_college[college_code]
            request_next = service_permissions.list_next(request, response)
            if request_next is not None:
                batch_next.add(request_next, request_id=college_code)
                request_by_college[college_code] = request_next
                need_batch = True
        else:
            # Handle error
            LOG.error('Google Drive API reported an error : %s', str(exception))

    batch_next = service.new_batch_http_request(callback=process_permissions_callback)
    for college_code, college in per_college_settings.items():
        drive_id = college['drive_id']
        user_perms_by_college[college_code] = dict()

        # Fetch all permissions for the college drive
        request = service_permissions.list(
            fileId=drive_id,
            fields='*',
            **COMMON_PARAMS
        )
        request_by_college[college_code] = request
        batch_next.add(request, request_id=college_code)

    while need_batch:
        need_batch = False
        batch_current = batch_next
        batch_next = service.new_batch_http_request(callback=process_permissions_callback)
        batch_current.execute()

    batch_current = None
    batch_next = None
    request_by_college = None

    LOG.info(f'Found {len(users)} college drive users')

    # Form the "others" group by removing all other group members (and the
    # account used to authorise this operation) from the set of all users
    # who currently have access to one or more college drives
    group_members = [set(group['users']) for group in groups.values()]
    groups['others'] = dict(
        name='others',
        users=list(
            users
            - {credentials.service_account_email}
            - set.union(*group_members)
        ),
    )

    # Check permission change requests refer to recognised groups and access
    # levels, and raise warnings for (and filter out) requests that don't
    verified_group_permissions = []
    for group_entry in group_permissions:
        group_name = group_entry['group_name']
        if not (group_name in groups):
            LOG.warning('Group "%s" not found in job specification', group_name)
            continue

        access_level = group_entry['access_level']
        if convert_access_level_to_api_role(access_level) is None:
            LOG.warning('Group "%s" cannot be assigned unrecognised access level "%s"',
                        group_name, access_level)
            continue

        verified_group_permissions.append(group_entry)
        LOG.warning('Request for group "%s" to be given access level "%s"',
                    group_name, group_entry['access_level'])

    with timeout_checker(pooldrive_settings) as has_timed_out:
        # Iterate over all colleges in the settings, generating batch entries to
        # update permissions as necessary
        operations = []
        for college_code, college in per_college_settings.items():
            LOG.info('Verifying group membership for college drive "%s"...', college_code)

            drive_id = college['drive_id']
            perms = user_perms_by_college[college_code]

            # Add batch entries to update roles (access levels) for all groups
            common_params = dict(
                credentials=credentials,
                drive_id=drive_id,
                college_code=college_code,
                perms=perms,
                service_permissions=service_permissions,
                operations=operations,
                dry_run=dry_run,
            )
            for group_entry in verified_group_permissions:
                group_name = group_entry['group_name']
                update_group_role(
                    group_name=group_name,
                    group=groups[group_name]['users'],
                    access_level=group_entry['access_level'],
                    **common_params
                )

            if has_timed_out():
                break

        # Actually update the permissions in batches

        # Counter that batch_request_callback() uses to communicate how many times the rate limit
        # has been exceeded.
        rate_limit_exceeded_count = 0

        def batch_request_callback(request_id, response, exception):
            """
            A callback method to handle any errors from the request batch
            """
            nonlocal rate_limit_exceeded_count
            if exception:
                # If the rate limit has been exceeded then count this and
                # append the failed operation to operations for re-sending.
                if b'userRateLimitExceeded' in exception.content:
                    rate_limit_exceeded_count += 1
                    operations.append(
                        operations[int(request_id)]
                    )
                else:
                    LOG.error('Google Drive API reported an error : %s', exception)

        send_message = (
            '%s %s operations in batch request(s) to update college shared drive permissions...'
        )
        if dry_run:
            LOG.info(send_message, 'Would send', len(operations))
        else:
            LOG.info(send_message, 'Sending', len(operations))
            batch = service.new_batch_http_request(callback=batch_request_callback)

        # How many of the immediate previous batches exceeded the rate limit
        # and controls the sleep (if any) between batches.
        retries = 0
        # How many operations are in any current batch.
        operation_count = 0
        # The total number of operations either batched for sending or sent (including re-sends).
        total_operation_count = 0

        while total_operation_count < len(operations) and not has_timed_out():
            if not dry_run:
                batch.add(operations[total_operation_count], request_id=str(total_operation_count))
            operation_count += 1
            total_operation_count += 1
            # We send a batch either when we're at the batch limit or if it's the last batch.
            # Note that we still loop on the last batch in case re-tries are appended by
            # batch_request_callback().
            if operation_count >= BATCH_LIMIT or total_operation_count == len(operations):
                if dry_run:
                    LOG.info(
                        'Would send batch request to update college shared drive permissions: ' +
                        '%d/%d operations', total_operation_count, len(operations)
                    )
                else:
                    # an exponential back-off is used as recommended here:
                    # ..cloud.google.com/iot/docs/how-tos/exponential-backoff#example_algorithm
                    if retries > 0:
                        duration = min(
                            (2 ** (retries - 1)) + random.uniform(0, 1), MAXIMUM_BACKOFF_DURATION
                        )
                        LOG.info('Sleeping for %s seconds', duration)
                        time.sleep(duration)
                    LOG.info(
                        'Sending batch request to update college shared drive permissions: ' +
                        '%d/%d operations', total_operation_count, len(operations)
                    )
                    batch.execute()
                    # Log any rate limit errors and increment or reset the retries depending on
                    # whether the rate limit was exceeded.
                    if rate_limit_exceeded_count > 0:
                        LOG.error(
                            '"User Rate Limit Exceeded" occured %s time(s). ' +
                            'Any failures are retried', rate_limit_exceeded_count
                        )
                        rate_limit_exceeded_count = 0
                        retries += 1
                    else:
                        retries = 0
                    batch = service.new_batch_http_request(callback=batch_request_callback)
                operation_count = 0

    LOG.info('Update of college shared drive permissions complete')


def update_group_role(
    credentials=None, drive_id=None, college_code=None, perms=None,
    service_permissions=None, operations=None, dry_run=False,
    group=None, group_name=None, access_level=None,
):
    # Update admin access level
    group_role = convert_access_level_to_api_role(access_level)
    if group_role is None:
        return

    for email_address in group:
        # Never alter permissions for the account used to authorise this
        # operation
        if credentials.service_account_email == email_address:
            continue

        if email_address in perms:
            perm = perms[email_address]
            permission_id = perm['id']
            role = perm['role']
            if access_level == 'none':
                # User in the group is already a member of the college drive,
                # so remove them from the drive
                if dry_run:
                    LOG.info('Would remove access of group "%s" member "%s" from college "%s"',
                             group_name, email_address, college_code)
                else:
                    LOG.info('Adding request to remove access of group ' +
                             '"%s" member "%s" from college "%s"',
                             group_name, email_address, college_code)
                    operations.append(service_permissions.delete(
                        fileId=drive_id,
                        permissionId=permission_id,
                        **COMMON_PARAMS
                    ))
            else:
                # User in the group is already a member of the college drive,
                # so change their access permissions
                if role != group_role:
                    prev_access_level = convert_api_role_to_access_level(role)
                    if dry_run:
                        LOG.info('Would change access level of group ' +
                                 '"%s" member "%s" from "%s" to "%s" for college "%s"',
                                 group_name, email_address, prev_access_level, access_level,
                                 college_code)
                    else:
                        LOG.info('Adding request to change access level of group ' +
                                 '"%s" member "%s" from "%s" to "%s" for college "%s"',
                                 group_name, email_address, prev_access_level, access_level,
                                 college_code)
                        operations.append(service_permissions.update(
                            fileId=drive_id,
                            permissionId=permission_id,
                            body={
                                'role': group_role
                            },
                            **COMMON_PARAMS
                        ))
        else:
            if access_level != 'none':
                # User in the group is not a member of the college drive,
                # so add them as a member
                if dry_run:
                    LOG.info('Would add access level of group ' +
                             '"%s" member "%s" to "%s" for college "%s"',
                             group_name, email_address, access_level, college_code)
                else:
                    LOG.info('Adding request to add access level of group ' +
                             '"%s" member "%s" to "%s" for college "%s"',
                             group_name, email_address, access_level, college_code)
                    operations.append(service_permissions.create(
                        fileId=drive_id,
                        sendNotificationEmail=False,
                        body={
                            'role': group_role,
                            'type': 'user',
                            'emailAddress': email_address,
                        },
                        **COMMON_PARAMS
                    ))


def convert_access_level_to_api_role(access_level):
    """
    Convert a GDrive access level (Manager, Viewer, etc.) to the role name
    used by the Google API.
    """

    if access_level is not None:
        access_level_lc = access_level.lower()
        if access_level_lc in ACCESS_LEVEL_TO_API_ROLE:
            return ACCESS_LEVEL_TO_API_ROLE[access_level_lc]
    return None


def convert_api_role_to_access_level(role):
    """
    Convert a role name used by the Google API to the GDrive access level
    (Manager, Viewer, etc.).
    """
    if role is not None:
        role_lc = role.lower()
        if role_lc in API_ROLE_TO_ACCESS_LEVEL:
            return API_ROLE_TO_ACCESS_LEVEL[role_lc]
    return None
