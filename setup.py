import os

from setuptools import setup, find_packages


def load_requirements():
    """
    Load requirements file and return non-empty, non-comment lines with leading and trailing
    whitespace stripped.
    """
    with open(os.path.join(os.path.dirname(__file__), "requirements", 'requirements.txt')) as f:
        return [
            line.strip() for line in f
            if line.strip() != '' and not line.strip().startswith('#')
        ]


setup(
    name='ugraddrivesync',
    version='0.0.2',
    packages=find_packages(),
    install_requires=load_requirements(),
    entry_points={
        'console_scripts': [
            'ugraddrivesync=ugraddrivesync.tool:main',
        ]
    },
    package_data={
        "ugraddrivesync": ["logging.conf"],
    },
)
