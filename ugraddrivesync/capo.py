"""
Download new CAPO documents from CamSIS and upload to the appopriate Google Drive folder for
each applicant.
"""

from base64 import b64decode
from io import BytesIO
import logging
import sys

from googleapiclient.discovery import build
from googleapiclient.http import MediaIoBaseUpload
import requests
from requests.auth import HTTPBasicAuth

from ugraddrivesync import COMMON_PARAMS, gapi_request, get_credentials
from ugraddrivesync import get_files_and_folders_for_drive, ProcessTimeout, timeout_checker
from ugraddrivesync.camsis import match_filter

LOG = logging.getLogger(__name__)

GOOGLE_SCOPES = [
    'https://www.googleapis.com/auth/drive',
    'https://www.googleapis.com/auth/drive.file',
    'https://www.googleapis.com/auth/drive.appfolder',
]

# Files list page size (this is also the maximum)
FILES_LIST_PAGE_SIZE = 1000

# A query clause for Google Drive API files list call limiting it only to folders
FOLDERS_ONLY_QUERY_CLAUSE = (
    "(mimeType = 'application/vnd.google-apps.folder')"
)

# A query clause for Google Drive API files list call limiting it only to CAPOs
CAPO_APP_PROP_KEY = 'auto_gen_file_type'
CAPO_APP_PROP_VALUE = 'capo_transcript'
CAPOS_ONLY_QUERY_CLAUSE = (
    f"(appProperties has {{ key='{CAPO_APP_PROP_KEY}' and value='{CAPO_APP_PROP_VALUE}' }})"
)

# CAPO document types (as returned by CamSIS)
CAPO_CAMSIS_FILE_TYPE_CAPO = 'CAPO'
CAPO_CAMSIS_FILE_TYPE_TRANSCRIPT = 'Transcript'
CAPO_CAMSIS_FILE_TYPE_MERGED = 'CAPO Merged with Transcript'

# App property used to store the CAPO document type (from CamSIS) with Google Drive API
CAPO_CAMSIS_FILE_TYPE_APP_PROP_KEY = 'capo_camsis_file_type'

# CAPO document combinations
CAPO_DOCS_NONE = 0
CAPO_DOCS_CAPO_ONLY = 1
CAPO_DOCS_TRANSCRIPT_ONLY = 2
CAPO_DOCS_COMPLETE = 3


def sync_with_google_drive(settings, *, dry_run=False):
    # Get an authenticated service for accessing Google Drive
    drive_credentials = get_credentials(settings, scopes=GOOGLE_SCOPES)
    drive_service = build('drive', 'v3', credentials=drive_credentials)
    files_service = drive_service.files()

    # Get job settings from the job specification
    capo_settings = settings.get('capo')
    if capo_settings is None:
        LOG.error('The CAPO settings must be provided.')
        sys.exit(1)

    camsis_settings = capo_settings.get('provider', {})
    camsis_url = camsis_settings.get('url')
    camsis_username = camsis_settings.get('username')
    camsis_password = camsis_settings.get('password')
    camsis_kwargs = (
        {} if None in [camsis_username, camsis_password] else
        {'auth': HTTPBasicAuth(camsis_username, camsis_password)}
    )
    camsis_filter = camsis_settings.get('filter')
    if camsis_url is None:
        LOG.error('The provider.url setting must be provided.')
        sys.exit(1)

    google_drive_settings = capo_settings['google_drive']

    # Start the main sync task
    with timeout_checker(capo_settings) as has_timed_out:
        # Get all the applicant folders indexed by UCAS PID so we know where to put each CAPO
        applicant_folders_by_ucas = get_applicant_folders_by_ucas(
            files_service, google_drive_settings, has_timed_out
        )

        # Find all auto-generated CAPOs currently on the drive (using the custom meta-data
        # properties to find them). The resulting dictionary is indexed by UCAS PID, with each
        # entry a CAPO_DOCS_* value indicating whether a CAPO and/or transcript were found.
        capos_by_ucas = get_capos_by_ucas(
            files_service, google_drive_settings, has_timed_out
        )

        # Fetch lists of applicants with generated CAPOs from CamSIS, download each CAPO document
        # and place in the correct applicant folder
        has_more = True
        next_url = camsis_url
        while has_more:
            # Fetch list of applicants with CAPOs
            response = requests.get(next_url, stream=True, **camsis_kwargs)
            response.raise_for_status()
            response_dict = response.json()

            applicants_response = response_dict.get('capoApplicantsResponse', {})
            has_more = applicants_response.get('hasMore', False)
            next_url = applicants_response.get('next', None)

            # Apply filter to remove responses that shouldn't be processed
            if not match_filter(camsis_filter, applicants_response):
                continue

            # For each applicant, get the list of CAPO documents (may be more than one if the
            # transcript is separate)
            capo_applicants = applicants_response.get('capoApplicants', [])
            for applicant in capo_applicants:
                ucas_pid = applicant.get('ucasPersonalId', None)
                camsis_number = applicant.get('applicationNumber', None)
                created_date_time = applicant.get('createdDateTime', None)
                file_url = applicant.get('fileUrl', None)
                if (
                    ucas_pid is None or camsis_number is None or created_date_time is None or
                    file_url is None
                ):
                    continue

                # Verify there is an applicant folder to place the CAPO in
                if ucas_pid not in applicant_folders_by_ucas:
                    LOG.warning(
                        'Could not find matching applicant folder for CAPO with ' +
                        f'UCAS PID {ucas_pid}'
                    )
                    continue

                # Skip applicants whose documents have already been uploaded
                capo_docs_status = capos_by_ucas.get(ucas_pid, CAPO_DOCS_NONE)
                if capo_docs_status == CAPO_DOCS_COMPLETE:
                    LOG.debug(
                        "Skipping already uploaded documents for applicant with " +
                        f"UCAS PID {ucas_pid}"
                    )
                    continue

                # For each file, locate the matching applicant folder and upload the file
                has_more_files = True
                next_file_url = file_url
                while has_more_files:
                    response = requests.get(next_file_url, stream=True, **camsis_kwargs)
                    response.raise_for_status()
                    response_dict = response.json()

                    files_response = response_dict.get('capoFilesResponse', {})
                    has_more_files = files_response.get('hasMore', False)
                    next_file_url = files_response.get('next', None)
                    file_type = files_response.get('type', 'Unknown Document')
                    file_content_base64 = files_response.get('base64Pdf', '')
                    file_content = b64decode(file_content_base64)

                    # For split CAPO and transcript, skip individual files if already uploaded
                    if (
                        (file_type == CAPO_CAMSIS_FILE_TYPE_CAPO and
                         capo_docs_status == CAPO_DOCS_CAPO_ONLY) or
                        (file_type == CAPO_CAMSIS_FILE_TYPE_TRANSCRIPT and
                         capo_docs_status == CAPO_DOCS_TRANSCRIPT_ONLY)
                    ):
                        continue

                    # Rename merged CAPO as it might not contain a transcript
                    if file_type == CAPO_CAMSIS_FILE_TYPE_MERGED:
                        doc_name = CAPO_CAMSIS_FILE_TYPE_CAPO
                    else:
                        doc_name = file_type

                    applicant_folders = applicant_folders_by_ucas[ucas_pid]
                    for applicant_folder in applicant_folders:
                        if dry_run:
                            LOG.info(
                                f"Would add {file_type} for applicant with UCAS PID {ucas_pid}"
                            )
                            continue

                        # Upload file to Google Drive folder
                        applicant_folder_name = applicant_folder['name']
                        media = MediaIoBaseUpload(
                            BytesIO(file_content), mimetype='application/pdf'
                        )
                        file_metadata = {
                            'appProperties': {
                                CAPO_APP_PROP_KEY: CAPO_APP_PROP_VALUE,
                                CAPO_CAMSIS_FILE_TYPE_APP_PROP_KEY: file_type,
                                'camsis_created_date_time': created_date_time,
                                'camsis_id': camsis_number,
                                'ucas_number': ucas_pid,
                            },
                            'description': f'{doc_name} for applicant with UCAS PID: {ucas_pid}',
                            'name': f'{doc_name} - {applicant_folder_name}.pdf',
                            'parents': [applicant_folder['id']],
                        }
                        gapi_request(lambda: files_service.create(
                            body=file_metadata,
                            fields='id',
                            media_body=media,
                            **COMMON_PARAMS
                        ).execute(), has_timed_out)
                        LOG.info(f"Added {file_type} for applicant with UCAS PID {ucas_pid}")

                if has_timed_out():
                    raise ProcessTimeout()


def get_applicant_folders_by_ucas(files_service, google_drive_settings, has_timed_out):
    """
    Gets a list of applicant folders (from gdrive) arranged by their applicant's UCAS PID
    """

    college_drives = google_drive_settings.get('colleges', [])

    default_drive_id = google_drive_settings['drive_id']
    default_folder_id = google_drive_settings.get('folder_id', default_drive_id)

    college_drives.append({
        'code': 'DEFAULT',
        'description': 'Default college drive',
        'drive_id': default_drive_id,
        'folder_id': default_folder_id
    })

    applicant_folders_by_ucas = {}
    for college_drive in college_drives:
        drive_id = college_drive['drive_id']
        folder_id = college_drive.get('folder_id', drive_id)
        LOG.debug(f'Retrieving applicant folders for drive {drive_id}, folder {folder_id}')

        for applicant_folder in get_applicant_folders(
            files_service,
            drive_id,
            folder_id,
            has_timed_out
        ):
            if 'appProperties' not in applicant_folder:
                LOG.warning(f"unrecognised folder: {applicant_folder['id']}")
                continue

            ucas_number = applicant_folder['appProperties']['ucas_number']
            applicant_folders_by_ucas.setdefault(ucas_number, [])
            applicant_folders_by_ucas[ucas_number].append(applicant_folder)

    return applicant_folders_by_ucas


def get_applicant_folders(files_service, drive_id, folder_id, has_timed_out):
    """
    Retrieve all of the applicant folders in a drive folder
    """
    page_token = None
    while True:
        LOG.debug('Retrieving page of applicant folders')
        response = gapi_request(lambda: files_service.list(
            q=(
                f"(trashed = false) and ('{folder_id}' in parents) and " +
                FOLDERS_ONLY_QUERY_CLAUSE
            ),
            fields='nextPageToken, '
                   'files('
                   'id, name, parents, appProperties, permissionIds'
                   ')',
            pageToken=page_token,
            corpora='drive',
            driveId=drive_id,
            includeItemsFromAllDrives=True,
            pageSize=FILES_LIST_PAGE_SIZE,
            **COMMON_PARAMS
        ).execute(), has_timed_out)
        for folder in response.get('files', []):
            yield folder
        page_token = response.get('nextPageToken', None)
        if page_token is None:
            break


def get_capos_by_ucas(files_service, google_drive_settings, has_timed_out):
    """
    Get existence of CAPOs from all drives, indexed by UCAS PID. Each entry is a CAPO_DOCS_*
    value indicating whether a CAPO and/or transcript were found.
    """

    # Create a list of all the searchable drives (all college drives and the default drive)
    default_drive_id = google_drive_settings.get('drive_id')
    searchable_drives_and_folders = [default_drive_id] + [
        record['drive_id'] for record in google_drive_settings.get('colleges', [])
    ]

    capos_by_ucas = {}
    for search_drive_id in searchable_drives_and_folders:
        LOG.debug(f'Retrieving CAPO info for drive {search_drive_id}')
        drive_files_and_folders = get_files_and_folders_for_drive(
            files_service, search_drive_id, CAPOS_ONLY_QUERY_CLAUSE, has_timed_out
        )

        for _, item in drive_files_and_folders.items():
            ucas_number = item['appProperties'].get('ucas_number', None)
            file_type = item['appProperties'].get(CAPO_CAMSIS_FILE_TYPE_APP_PROP_KEY, None)

            if file_type == CAPO_CAMSIS_FILE_TYPE_MERGED:
                capos_by_ucas[ucas_number] = CAPO_DOCS_COMPLETE
            elif file_type == CAPO_CAMSIS_FILE_TYPE_CAPO:
                if ucas_number in capos_by_ucas:
                    if capos_by_ucas[ucas_number] == CAPO_DOCS_TRANSCRIPT_ONLY:
                        capos_by_ucas[ucas_number] = CAPO_DOCS_COMPLETE
                else:
                    capos_by_ucas[ucas_number] = CAPO_DOCS_CAPO_ONLY
            elif file_type == CAPO_CAMSIS_FILE_TYPE_TRANSCRIPT:
                if ucas_number in capos_by_ucas:
                    if capos_by_ucas[ucas_number] == CAPO_DOCS_CAPO_ONLY:
                        capos_by_ucas[ucas_number] = CAPO_DOCS_COMPLETE
                else:
                    capos_by_ucas[ucas_number] = CAPO_DOCS_TRANSCRIPT_ONLY

    return capos_by_ucas
