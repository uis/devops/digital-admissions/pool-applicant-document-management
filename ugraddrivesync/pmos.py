import logging

import gspread
from gspread.exceptions import APIError
from gspread.cell import Cell
from gspread.utils import a1_to_rowcol, absolute_range_name, cast_to_a1_notation, fill_gaps, \
                          rowcol_to_a1

from ugraddrivesync import api_request, fetch_applications, fetch_json, get, get_credentials, \
                           get_required_setting, timeout_checker, post_json, ProcessTimeout

LOG = logging.getLogger(__name__)

# Google Drive API scopes
SCOPES = [
    'https://spreadsheets.google.com/feeds',
    'https://www.googleapis.com/auth/drive',
]

# ID fields of applications
CAMSIS_ID_FIELD_NAME = 'camsisApplicationNumber'
UCAS_ID_FIELD_NAME = 'candidate.ucasPersonalId'

# UUIDs of application annotations
GENDER_FIELD_UUID = 'fbf62e06-c4e2-11e9-b133-0b0c864924b7'
MATURE_FIELD_UUID = '7a04a89f-2d27-4c4c-bbbb-2184bc0d5855'

# Index of the worksheet to use in each PMOS
WORKSHEET_INDEX = 0

# Title of the main worksheet in each PMOS
WORKSHEET_TITLE = 'PMOS'

# Max updates/new rows per Google Sheets API batch request
MAX_BATCH_ENTRIES = 10000

# Maps the only valid pool outcome statuses that can be set in the sheet to their IDs.
POOL_OUTCOME_STATUS_DESCRIPTION_TO_CODE = {
    'Offer': 'OFFER',
    'Interview': 'INTERVIEW',
    'Accepted': 'ACCEPT',
}

# Defines the pool outcome creation fields. The key is the creation field and the values it's
# default application field, if it has one.
POOL_OUTCOME_CREATION_FIELDS = {
    "status": None,
    "collegePreference": "collegePreference",
    "admitYear": "admitYear",
    "subject": "subject",
    "interviewCollege1": None,
    "interviewCollege2": None,
}

# Defines the header text for key columns
POOL_OUTCOME_HEADER_TEXT = 'Pool Outcome'
COLLEGE_HEADER_TEXT_WINTER_POOL = 'Offer College'
COLLEGE_HEADER_TEXT_SUMMER_POOL = 'Accepting College'
YEAR_OF_ENTRY_CHANGE_HEADER_TEXT = 'Year of Entry Change (if applicable)'
COURSE_CHANGE_HEADER_TEXT = 'Course Change (if applicable)'
INTERVIEW_COLLEGE_1_HEADER_TEXT = 'Interview College 1'
INTERVIEW_COLLEGE_2_HEADER_TEXT = 'Interview College 2'

# Defines the default column width for the sheet
DEFAULT_COLUMN_WIDTH = 100


def get_latest_pool_outcome(application):
    """
    Returns an application's latest pool outcome (if it has one) as a creation `dict`. If not, the
    creation `dict` is built from application defaults.
    """
    if application.get('latestPoolOutcome') is not None:
        return {
            field: application['latestPoolOutcome'][field]
            for field in POOL_OUTCOME_CREATION_FIELDS
        }

    return {
        key: application.get(value) for key, value in POOL_OUTCOME_CREATION_FIELDS.items()
    }


class CommonData:
    """
    An object that is initialised with data common to both the import and export process.
    """
    def __init__(self, url_application_counts, headers, pool_type, *args, **kwargs):

        summer_pool = pool_type == 'summer'

        # Fetch information about permitted values (plus descriptions) for college codes
        self.college_counts = fetch_json(
            url=url_application_counts,
            query=dict(secondAxis='collegePreference'),
            headers=headers
        )
        raw_college_descriptions = [
            x['secondAxisDescription'] for x in self.college_counts.get('secondAxisCounts', [])
        ]
        college_descriptions = list(filter(
            lambda x: x != 'Open Application', raw_college_descriptions
        ))
        college_descriptions.sort()

        # Fetch information about permitted values (plus descriptions) for course/subject codes
        self.subject_counts = fetch_json(
            url=url_application_counts,
            query=dict(secondAxis='subject'),
            headers=headers
        )
        subject_descriptions = [
            x['secondAxisDescription'] for x in self.subject_counts.get('secondAxisCounts', [])
        ]
        subject_descriptions.sort()

        # Fetch information about permitted values entry years
        self.entry_year_counts = fetch_json(
            url=url_application_counts,
            query=dict(secondAxis='admitYear'),
            headers=headers
        )
        entry_years = [
            x['secondAxisId'] for x in self.entry_year_counts.get('secondAxisCounts', [])
        ]
        entry_years.sort()

        # only a field in the summer pool sheets
        pool_type_description = [
            dict(field='poolTypeDescription',
                 row_header='Pool Type',
                 import_fn=lambda x: "" if x == 'None' else x),
        ] if summer_pool else []

        # List of spreadsheet columns that display data from the SMI
        self.columns_from_smi = [
            dict(field=UCAS_ID_FIELD_NAME,
                 row_header='UCAS Personal ID',
                 width=120),
            dict(field='candidate.lastName',
                 row_header='Last Name',
                 width=120),
            dict(field='candidate.forenames',
                 row_header='Forenames',
                 width=200),
            dict(field='subjectOptions',
                 row_header='Pool Course Group',
                 width=150,
                 import_fn=lambda x: ", ".join(x)),
            dict(field='collegePreferenceDescription',
                 row_header='Pooling College',
                 width=200),
            *pool_type_description,
            dict(field='admitYear',
                 row_header='Year of Entry'),
            dict(field='annotations',
                 row_header='Mature',
                 width=70,
                 import_fn=get_annotation_import_fn(MATURE_FIELD_UUID)),
            dict(field='annotations',
                 row_header='Gender',
                 width=70,
                 import_fn=get_annotation_import_fn(GENDER_FIELD_UUID)),
            dict(field=None,
                 row_header=POOL_OUTCOME_HEADER_TEXT,
                 width=200,
                 dropdown_list=college_descriptions,
                 import_fn=get_pool_outcome_import_fn(summer_pool)),
        ]

        self.ucas_id_column_index = next((
            index for (index, column) in enumerate(self.columns_from_smi)
            if column['field'] == UCAS_ID_FIELD_NAME
        ))

        # only fields in the winter pool sheets
        interview_colleges = [] if summer_pool else [
            dict(field=None,
                 row_header=INTERVIEW_COLLEGE_1_HEADER_TEXT,
                 width=200,
                 dropdown_list=college_descriptions),
            dict(field=None,
                 row_header=INTERVIEW_COLLEGE_2_HEADER_TEXT,
                 width=200,
                 dropdown_list=college_descriptions),
        ]

        # pool type dependent field name
        self.college_header_text = (
            COLLEGE_HEADER_TEXT_SUMMER_POOL
            if summer_pool else
            COLLEGE_HEADER_TEXT_WINTER_POOL
        )

        # List of spreadsheet columns that should result in data being sent to the SMI (data comes
        # from PMOS)
        self.columns_from_pmos = [
            dict(field='latestPoolOutcome.collegePreference',
                 row_header=self.college_header_text,
                 width=200,
                 dropdown_list=college_descriptions),
            dict(field='latestPoolOutcome.admitYear',
                 row_header=YEAR_OF_ENTRY_CHANGE_HEADER_TEXT,
                 width=240,
                 dropdown_list=entry_years),
            dict(field='latestPoolOutcome.subject',
                 row_header=COURSE_CHANGE_HEADER_TEXT,
                 width=200,
                 dropdown_list=subject_descriptions),
            *interview_colleges
        ]

        # List of spreadsheet column headers
        self.all_columns = self.columns_from_smi + self.columns_from_pmos
        self.header = [column['row_header'] for column in self.all_columns]
        self.num_columns = len(self.header)


def get_annotation_import_fn(annotation_uuid):
    """
    Return a function that takes an application and extracts the value of the annotation whose
    type matches annotation_uuid.
    """
    def annotation_import_fn(annotations):
        result = list(filter(
            lambda x: x.get('type', {}).get('id', '') == annotation_uuid,
            annotations if annotations else []
        ))
        if len(result) == 1:
            return result[0].get('value', '')
        return ''
    return annotation_import_fn


def get_pool_outcome_import_fn(summer_pool):
    """
    Return a function that returns a formula that calculates the pool outcome based on the values
    of other cells. Different depending on the pool type.
    """
    if summer_pool:
        def pool_outcome_export_fn(_):
            return '=IF(ISBLANK(INDIRECT(ADDRESS(ROW(), 11, 4))), "", "Accepted")'
    else:
        def pool_outcome_export_fn(_):
            return '=IF(ISBLANK(INDIRECT(ADDRESS(ROW(), 10, 4))), ' \
                'IF(and(ISBLANK(INDIRECT(ADDRESS(ROW(), 13, 4))),' \
                'ISBLANK(INDIRECT(ADDRESS(ROW(), 14, 4)))), "", "Interview"), "Offer")'

    return pool_outcome_export_fn


def import_smi_data(settings, pool_type, *, dry_run=False):
    """
    Synchronise a list of applications queried from SMI with each "Poolside Meeting Outcome
    Spreadsheet" Google spreadsheet.

    """
    # Configure applicant source
    pmos_settings = settings.get(f'pmos_{pool_type}')
    source_settings = pmos_settings.get('source')
    source_url_applications = source_settings.get('url_applications')
    source_url_application_counts = source_settings.get('url_application_counts')
    source_queries = source_settings.get('queries', [])
    source_headers = source_settings.get('headers', {})

    # Configure destination spreadsheets
    pmos_sheets = pmos_settings.get('sheets')

    # Initialise the common data
    common = CommonData(source_url_application_counts, source_headers, pool_type)

    num_columns_from_smi = len(common.columns_from_smi)

    # Fetch all the applications we need to sync. Add each application to a dictionary (to ensure
    # the uniqueness of each application), and then convert to a list of all applications.
    all_applications = []
    applications_by_pmos_key = {}

    LOG.info('Fetching all applications')
    source_applications = {}
    for query in source_queries:
        for application in fetch_applications(
            url=source_url_applications, query=query, headers=source_headers
        ):
            source_applications[application['id']] = application
    all_applications = source_applications.values()
    all_applications = sorted(
        all_applications,
        key=lambda x: (
            x.get('candidate', {}).get('lastName', ''),
            x.get('candidate', {}).get('forenames', '')
        )
    )

    source_applications = None

    # Add each application to the list of each matched PMOS (if any)
    for application in all_applications:
        matched_pmos_sheet = None
        for pmos_sheet in pmos_sheets:
            pmos_filters = pmos_sheet['filter']
            for pmos_filter in pmos_filters:
                matched_filter = True
                for filter_prop, filter_value in pmos_filter.items():
                    if application[filter_prop] != filter_value:
                        matched_filter = False
                        break
                if matched_filter:
                    matched_pmos_sheet = pmos_sheet
                    break
            if matched_pmos_sheet is not None:
                applications = applications_by_pmos_key.setdefault(
                    matched_pmos_sheet['key'], []
                )
                applications.append(application)
                break

    LOG.info('Fetched %s applications', len(all_applications))

    # Gain authority to access the PMOSs
    credentials = get_credentials(settings, scopes=SCOPES)
    gc = gspread.authorize(credentials)

    # Process each PMOS
    with timeout_checker(pmos_settings) as has_timed_out:
        for pmos_sheet in pmos_sheets:
            pmos_sheet_key = pmos_sheet.get('key')
            pmos_sheet_description = pmos_sheet.get('description')
            LOG.info(f'Starting update of PMOS {str(pmos_sheet_description)} {pmos_sheet_key}')
            spreadsheet = gspread_request(
                lambda: gc.open_by_key(pmos_sheet_key), has_timed_out
            )
            spreadsheet_metadata = gspread_request(
                lambda: spreadsheet.fetch_sheet_metadata(), has_timed_out
            )
            worksheet_metadata = next(
                x for x in spreadsheet_metadata.get('sheets')
                if x.get('properties', {}).get('index') == 0
            )
            worksheet = gspread_request(
                lambda: spreadsheet.get_worksheet(WORKSHEET_INDEX), has_timed_out
            )
            applications = applications_by_pmos_key.get(pmos_sheet_key, [])

            # Create a map of the application rows keyed on UCAS ID
            all_rows = gspread_request(
                lambda: range_formula(worksheet, 1, 1, worksheet.row_count, common.num_columns),
                has_timed_out
            )
            rows_by_id = {
                str(row[common.ucas_id_column_index].value): row
                for row in chunks(all_rows, common.num_columns, skip_rows=1)
                if row[common.ucas_id_column_index].value
            }

            # Get a count of the rows currently populated
            current_row_count = len(rows_by_id)

            # Changes to existing rows. A list of range updates of the form
            # {'range': a1_range, 'values': [[], [], ..]} for submission to
            # gspread.models.Worksheet.batch_update().
            updates = []

            # List of spreadsheet rows, each row being a list of strings where each string is a
            # column value
            new_rows = []

            # Match each application to a row and generate an update for its values if any values
            # are incorrect. If no row can be matched against then add to the list of new rows to
            # create.
            for application in applications:
                row = rows_by_id.pop(get(application, UCAS_ID_FIELD_NAME), None)
                new_values = create_row_values(application, common.columns_from_smi)
                if row is None:
                    new_rows.append(new_values)
                else:
                    row_diff = diff(row, new_values)
                    if len(row_diff) > 0:
                        updates.extend(row_diff)

            spreadsheet_title = spreadsheet_metadata.get('properties').get('title')
            LOG.info('%s: %s applications to add', spreadsheet_title, len(new_rows))
            LOG.info('%s: %s changes to make to application rows', spreadsheet_title, len(updates))

            # Verify header is correct and generate updates if necessary
            cur_header = list(filter(lambda x: x.row == 1, all_rows))
            header_diff = diff(cur_header, common.header)
            if len(header_diff) > 0:
                updates.extend(header_diff)

            # If this is a dry run then finish here
            if dry_run:
                LOG.info('dry run mode - no changes made')
                return

            # Calculate the number of rows that will contain applications (even if they're no
            # longer assigned to this PMOS according to the SMI)
            num_applications = len(applications)
            new_row_count = (
                num_applications if num_applications > current_row_count else current_row_count
            )

            # See if there are any empty rows at the end of the sheet
            empty_rows = worksheet.row_count - current_row_count - 1

            # Fill empty rows with as many applications as will fit
            new_rows_using_batch_update = new_rows[:empty_rows]
            if new_rows_using_batch_update:
                start_row = current_row_count + 2
                end_row = start_row + len(new_rows_using_batch_update) - 1
                updates.append({
                    'range': a1_range(start_row, 1, end_row, len(common.columns_from_smi)),
                    'values': new_rows_using_batch_update
                })

            # Update the spreadsheet with header changes, changes to existing applications and new
            # applications in existing empty rows.
            LOG.info('Updating spreadsheet rows...')
            while len(updates) > 0:
                gspread_request(
                    lambda: worksheet.batch_update(
                        updates[:MAX_BATCH_ENTRIES], value_input_option='USER_ENTERED'
                    ), has_timed_out
                )
                updates = updates[MAX_BATCH_ENTRIES:]

            # Append any remaining new applications to the sheet.
            LOG.info('Adding new spreadsheet rows...')
            new_rows_using_append_rows = new_rows[empty_rows:]
            while len(new_rows_using_append_rows) > 0:
                gspread_request(
                    lambda: worksheet.append_rows(
                        new_rows_using_append_rows[:MAX_BATCH_ENTRIES],
                        value_input_option='USER_ENTERED'
                    ), has_timed_out
                )
                new_rows_using_append_rows = new_rows_using_append_rows[MAX_BATCH_ENTRIES:]

            # For each column of user choices, use the setDataValidation operation to create
            # dropdown selectors with permitted values.  Generate low-level spreadsheet API
            # requests to achieve this (and for all subsequent operations in this function).
            #
            # Note that the start indexes of the rows and columns are non-inclusive, while the end
            # indexes are inclusive. So startRowIndex:2, endRowIndex:3 will only affect row 3.
            update_requests = []
            for column_list_index, column in enumerate(common.columns_from_pmos):
                column_index = num_columns_from_smi + column_list_index
                update_requests.append(create_data_validation_update(
                    worksheet_index=WORKSHEET_INDEX,
                    start_row_index=1,
                    end_row_index=new_row_count + 1,
                    start_column_index=column_index,
                    end_column_index=column_index + 1,
                    values=column.get('dropdown_list', [])
                ))

            # Set the width of each column
            for column_list_index, column in enumerate(common.all_columns):
                width = column.get('width', DEFAULT_COLUMN_WIDTH)
                update_requests.append({
                    'updateDimensionProperties': {
                        'range': {
                            'sheetId': WORKSHEET_INDEX,
                            'dimension': 'COLUMNS',
                            'startIndex': column_list_index,
                            'endIndex': column_list_index + 1
                        },
                        'fields': 'pixelSize',
                        'properties': {
                            'pixelSize': width
                        }
                    }
                })

            # Freeze the first row (the header) and the first 3 columns (ucas pid and name)
            update_requests.append({
                'updateSheetProperties': {
                    'properties': {
                        'sheetId': WORKSHEET_INDEX,
                        'gridProperties': {
                            'frozenRowCount': 1,
                            'frozenColumnCount': 3
                        }
                    },
                    'fields': 'gridProperties.frozenRowCount,gridProperties.frozenColumnCount'
                }
            })

            # Style the header
            update_requests.append({
                'repeatCell': {
                    'range': {
                        'sheetId': WORKSHEET_INDEX,
                        'startRowIndex': 0,
                        'endRowIndex': 1
                    },
                    'cell': {
                        'userEnteredFormat': {
                            'textFormat': {
                                'foregroundColor': {
                                    'red': 0.0,
                                    'green': 0.0,
                                    'blue': 0.0
                                },
                                'bold': True,
                                'underline': True
                            }
                        }
                    },
                    'fields': 'userEnteredFormat(textFormat)'
                }
            })

            # Style the application rows
            update_requests.append({
                'repeatCell': {
                    'range': {
                        'sheetId': WORKSHEET_INDEX,
                        'startRowIndex': 1,
                        'endRowIndex': new_row_count + 1,
                        'startColumnIndex': 0,
                        'endColumnIndex': common.num_columns,
                    },
                    'cell': {
                        'userEnteredFormat': {
                            'horizontalAlignment': 'LEFT'
                        }
                    },
                    'fields': 'userEnteredFormat(horizontalAlignment)'
                }
            })

            # Ensure the document has the expected name (if one has been provided)
            pmos_sheet_description = pmos_sheet.get('description')
            spreadsheet_title_matches = (
                spreadsheet_title == pmos_sheet_description
            )
            if pmos_sheet_description is not None and not spreadsheet_title_matches:
                update_requests.append({
                    'updateSpreadsheetProperties': {
                        'properties': {
                            'title': pmos_sheet_description
                        },
                        'fields': 'title'
                    }
                })

            # Ensure the worksheet has the expected name
            worksheet_title_matches = (
                worksheet_metadata.get('properties').get('title') == WORKSHEET_TITLE
            )
            if not worksheet_title_matches:
                update_requests.append({
                    'updateSheetProperties': {
                        'properties': {
                            'sheetId': WORKSHEET_INDEX,
                            'title': WORKSHEET_TITLE
                        },
                        'fields': 'title'
                    }
                })

            # Set alternating colour background (banded range) for the applications. Compare first
            # to see if the banded range already exists in the metadata, and create it if it not.
            # Otherwise compare the banded range to the one that's expected, and update it if they
            # differ.
            cur_banded_ranges = worksheet_metadata.get('bandedRanges', [])
            cur_banded_range = next(
                (x for x in cur_banded_ranges if x.get('bandedRangeId') == 1), None
            )
            banded_range = {
                'bandedRangeId': 1,
                'range': {
                    'startRowIndex': 0,
                    'endRowIndex': new_row_count + 1,
                    'startColumnIndex': 0,
                    'endColumnIndex': common.num_columns,
                },
                'rowProperties': {
                    'headerColor': {
                        'red': 0.7490196,
                        'green': 0.7490196,
                        'blue': 0.7490196
                    },
                    'firstBandColor': {
                        'red': 1,
                        'green': 1,
                        'blue': 1
                    },
                    'secondBandColor': {
                        'red': 0.9490196,
                        'green': 0.9490196,
                        'blue': 0.9490196
                    }
                }
            }

            if cur_banded_range is None:
                # Banded range doesn't exist, so create it
                banded_range['range']['sheetId'] = WORKSHEET_INDEX
                update_requests.append({
                    'addBanding': {
                        'bandedRange': banded_range
                    }
                })
            else:
                # Banded range exists, so verify it's what we expect and update it if it isn't
                range_matches = (
                    cur_banded_range.get('range') == banded_range.get('range') and
                    (cur_banded_range.get('rowProperties', {}).get('headerColor') ==
                     banded_range.get('rowProperties', {}).get('headerColor')) and
                    (cur_banded_range.get('rowProperties', {}).get('firstBandColor') ==
                     banded_range.get('rowProperties', {}).get('firstBandColor')) and
                    (cur_banded_range.get('rowProperties', {}).get('secondBandColor') ==
                     banded_range.get('rowProperties', {}).get('secondBandColor'))
                )
                if not range_matches:
                    banded_range['range']['sheetId'] = WORKSHEET_INDEX
                    update_requests.append({
                        'updateBanding': {
                            'bandedRange': banded_range,
                            'fields': 'range,rowProperties'
                        }
                    })

            # Protect the header and columns whose values come from the SMI. Compare first
            # to see if the protected ranges already exist in the metadata, and create them if
            # not. Otherwise compare the protected ranges to the ones that are expected, and
            # update them if they differ.
            #
            # For unknown reasons, protectedRangeId 1 could not be used when testing.
            cur_protected_ranges = worksheet_metadata.get('protectedRanges', [])
            cur_protected_header = next(
                (x for x in cur_protected_ranges if x.get('protectedRangeId') == 2), None
            )
            cur_protected_columns = next(
                (x for x in cur_protected_ranges if x.get('protectedRangeId') == 3), None
            )
            protected_header = {
                'protectedRangeId': 2,
                'description': 'Protected header',
                'editors': {
                    'users': [
                        credentials.service_account_email
                    ]
                },
                'range': {
                    'startRowIndex': 0,
                    'endRowIndex': 1,
                    'startColumnIndex': 0,
                    'endColumnIndex': common.num_columns,
                }
            }
            protected_columns = {
                'protectedRangeId': 3,
                'description': 'Protected values from the SMI',
                'editors': {
                    'users': [
                        credentials.service_account_email
                    ]
                },
                'range': {
                    'startRowIndex': 1,
                    'endRowIndex': new_row_count + 1,
                    'startColumnIndex': 0,
                    'endColumnIndex': num_columns_from_smi,
                }
            }

            if cur_protected_header is None:
                # Protected header doesn't exist, so create it
                protected_header['range']['sheetId'] = WORKSHEET_INDEX
                update_requests.append({
                    'addProtectedRange': {
                        'protectedRange': protected_header
                    }
                })
            else:
                # Protected header exists, so verify it's what we expect and update it if it isn't
                range_matches = (
                    cur_protected_header.get('range') == protected_header.get('range') and
                    (cur_protected_header.get('description') ==
                     protected_header.get('description')) and
                    (cur_protected_header.get('editors', {}).get('users') ==
                     protected_header.get('editors', {}).get('users')) and
                    (cur_protected_header.get('editors', {}).get('domainUsersCanEdit') ==
                     protected_header.get('editors', {}).get('domainUsersCanEdit'))
                )
                if not range_matches:
                    protected_header['range']['sheetId'] = WORKSHEET_INDEX
                    update_requests.append({
                        'updateProtectedRange': {
                            'protectedRange': protected_header,
                            'fields': 'description,editors,range'
                        }
                    })

            if cur_protected_columns is None:
                # Protected columns doesn't exist, so create it
                protected_columns['range']['sheetId'] = WORKSHEET_INDEX
                update_requests.append({
                    'addProtectedRange': {
                        'protectedRange': protected_columns
                    }
                })
            else:
                # Protected columns exists, so verify it's what we expect and update it if it isn't
                range_matches = (
                    cur_protected_columns.get('range') == protected_columns.get('range') and
                    (cur_protected_columns.get('description') ==
                     protected_columns.get('description')) and
                    (cur_protected_columns.get('editors', {}).get('users') ==
                     protected_columns.get('editors', {}).get('users')) and
                    (cur_protected_columns.get('editors', {}).get('domainUsersCanEdit') ==
                     protected_columns.get('editors', {}).get('domainUsersCanEdit'))
                )
                if not range_matches:
                    protected_columns['range']['sheetId'] = WORKSHEET_INDEX
                    update_requests.append({
                        'updateProtectedRange': {
                            'protectedRange': protected_columns,
                            'fields': 'description,editors,range'
                        }
                    })

            # Apply all low-level spreadsheet API requests
            if len(update_requests) > 0:
                LOG.info('Updating spreadsheet formatting and metadata...')
                gspread_request(
                    lambda: spreadsheet.batch_update(dict(requests=update_requests)),
                    has_timed_out
                )
            LOG.info(f'Completed update of PMOS {str(pmos_sheet_description)} {pmos_sheet_key}')

            # If the timeout fired while processing the PMOS then don't process any more
            if has_timed_out():
                raise ProcessTimeout()


def export_smi_data(settings, pool_type, *, dry_run=False):
    """
    Synchronise the data inputted into each "Poolside Meeting Outcome Spreadsheet" Google
    spreadsheet with the SMI. Changes in the sheet result in the creation of pool outcomes
    in the SMI.

    """
    # Configure applicant source (that we are writing back to)
    pmos_settings = get_required_setting(settings, f'pmos_{pool_type}')
    source_url_applications = get_required_setting(pmos_settings, 'source.url_applications')
    source_url_application_counts = get_required_setting(
        pmos_settings, 'source.url_application_counts'
    )
    source_url_pool_outcomes = get_required_setting(pmos_settings, 'source.url_pool_outcomes')
    source_queries = get(pmos_settings, 'source.queries', [])
    source_headers = get(pmos_settings, 'source.headers', {})

    # Configure spreadsheets
    pmos_sheets = get_required_setting(pmos_settings, 'sheets')

    # Initialise the common data
    common = CommonData(source_url_application_counts, source_headers, pool_type)

    college_description_to_code = {
        secondAxisCount['secondAxisDescription']: secondAxisCount['secondAxisId']
        for secondAxisCount in common.college_counts.get('secondAxisCounts', [])
    }

    subject_description_to_code = {
        secondAxisCount['secondAxisDescription']: secondAxisCount['secondAxisId']
        for secondAxisCount in common.subject_counts.get('secondAxisCounts', [])
    }

    # we use this to validate sheet entry years
    valid_entry_years = {
        secondAxisCount['secondAxisDescription']: secondAxisCount['secondAxisId']
        for secondAxisCount in common.entry_year_counts.get('secondAxisCounts', [])
    }

    # only fields in the winter pool sheet
    interview_college_converters = {
        "interviewCollege1": lambda _, row: (
            college_description_to_code[
                row[common.header.index(INTERVIEW_COLLEGE_1_HEADER_TEXT)].value
            ]
            if row[common.header.index(INTERVIEW_COLLEGE_1_HEADER_TEXT)].value
            else None
        ),
        "interviewCollege2": lambda _, row: (
            college_description_to_code[
                row[common.header.index(INTERVIEW_COLLEGE_2_HEADER_TEXT)].value
            ]
            if row[common.header.index(INTERVIEW_COLLEGE_2_HEADER_TEXT)].value
            else None
        ),
    } if pool_type == 'winter' else {}

    # A `dict` of converter lambdas, one for each editable field, that convert sheet fields to
    # pool outcome creation values. Note that, for `collegePreference`, `admitYear`, & `subject`,
    # the  `application` value is used, if blank.
    pool_outcome_converters = {
        "status": lambda application, row: (
            POOL_OUTCOME_STATUS_DESCRIPTION_TO_CODE[
                row[common.header.index(POOL_OUTCOME_HEADER_TEXT)].value
            ]
            if row[common.header.index(POOL_OUTCOME_HEADER_TEXT)].value
            else None
        ),
        "collegePreference": lambda application, row: (
            college_description_to_code[row[common.header.index(common.college_header_text)].value]
            if row[common.header.index(common.college_header_text)].value
            else application["collegePreference"]
        ),
        "admitYear": lambda application, row: (
            int(valid_entry_years[
                row[common.header.index(YEAR_OF_ENTRY_CHANGE_HEADER_TEXT)].value
            ])
            if row[common.header.index(YEAR_OF_ENTRY_CHANGE_HEADER_TEXT)].value
            else application["admitYear"]
        ),
        "subject": lambda application, row: (
            subject_description_to_code[row[common.header.index(COURSE_CHANGE_HEADER_TEXT)].value]
            if row[common.header.index(COURSE_CHANGE_HEADER_TEXT)].value
            else application["subject"]
        ),
        **interview_college_converters
    }

    # Fetch all the query applications and populate a map with them keyed on the UCAS id.

    applications_by_ucas_id = {}

    LOG.info('Fetching all applications')
    fetch_applications_kwargs = dict(url=source_url_applications, headers=source_headers)
    for query in source_queries:
        for application in fetch_applications(query=query, **fetch_applications_kwargs):
            applications_by_ucas_id[application['candidate']['ucasPersonalId']] = application

    LOG.info('Fetched %s applications', len(applications_by_ucas_id))

    # Open the spreadsheet
    credentials = get_credentials(settings, scopes=SCOPES)
    gc = gspread.authorize(credentials)

    new_pool_outcomes = 0

    with timeout_checker(pmos_settings) as has_timed_out:

        # For each PMOS..
        for sheet in pmos_sheets:
            pmos_sheet_key = get_required_setting(sheet, 'key')
            pmos_sheet_description = get_required_setting(sheet, 'description')
            LOG.info(f'Starting export of PMOS {pmos_sheet_description}')
            spreadsheet = gspread_request(
                lambda: gc.open_by_key(pmos_sheet_key), has_timed_out
            )
            worksheet = gspread_request(
                lambda: spreadsheet.get_worksheet(WORKSHEET_INDEX),
                has_timed_out
            )
            # get all the worksheet data
            all_rows = gspread_request(
                lambda: worksheet.range(2, 1, worksheet.row_count, common.num_columns),
                has_timed_out
            )
            # and for each row..
            for row in chunks(all_rows, common.num_columns):
                # try to match to an application
                matched_application = applications_by_ucas_id.get(
                    row[common.ucas_id_column_index].value
                )
                if matched_application:
                    # check for any changes to the pool outcome (or the default pool outcome)
                    latest_pool_outcome = get_latest_pool_outcome(matched_application)
                    diff = {}
                    for name, convert in pool_outcome_converters.items():
                        value = convert(matched_application, row)
                        if value != latest_pool_outcome.get(name):
                            diff[name] = value
                    # if there are changes then create a new pool outcome
                    if diff:
                        result = post_json(source_url_pool_outcomes, {
                            **latest_pool_outcome, **diff,
                            "applicationId": matched_application['id']
                        }, headers=source_headers, dry_run=dry_run)
                        if result or dry_run:
                            new_pool_outcomes += 1

    if dry_run:
        LOG.info(f'Pool outcomes that would be created: {new_pool_outcomes}')
    else:
        LOG.info(f'Pool outcomes created: {new_pool_outcomes}')


def gspread_request(request_fn, has_timed_out):
    """Specialisation of `api_request()` for making calls with the `gspread` client."""
    return api_request(request_fn, has_timed_out, APIError, lambda e: e.response.status_code)


def create_row_values(application, columns):
    """
    Return a list of spreadsheet row values based on the passed SMI application and a list of
    columns. Each column must be a dict() where the value of the "field" property is the name
    of the application property to use to populate the row value for the column.
    """
    row_values = []
    for column in columns:
        field = column.get('field')
        if field is None:
            value = ''
        else:
            value = get(application, field)
        if column.get('import_fn') is not None:
            row_values.append(column.get('import_fn')(value))
        else:
            row_values.append(str(value))
    return row_values


def create_value_update(a1_range, values):
    """
    Create a cell range value update that can be submitted to
    gspread.models.Worksheet.batch_update().
    """
    return {'range': a1_range, 'values': values}


def create_data_validation_update(
    worksheet_index=0, start_row_index=0, end_row_index=1,
    start_column_index=0, end_column_index=1, values=[]
):
    """
    Create a cell range value update that can be submitted to
    gspread.models.Spreadsheet.batch_update() (as an entry in the "requests" list)
    """
    return {
        'setDataValidation': {
            'range': {
                'sheetId': worksheet_index,
                'startRowIndex': start_row_index,
                'endRowIndex': end_row_index,
                'startColumnIndex': start_column_index,
                'endColumnIndex': end_column_index
            },
            'rule': {
                'condition': {
                    'type': 'ONE_OF_LIST',
                    'values': [dict(userEnteredValue=value) for value in values]
                },
                'showCustomUi': True,
                'strict': True
            }
        }
    }


def diff(row, values):
    """
    Diffs an existing row of sheet cell with a list of values. Any diffs are return as an update
    instruction (a range and value) that can be submitted to
    gspread.models.Worksheet.batch_update().
    """
    updates = []
    for cell, value in zip(row, values):
        if str(cell.value) != value:
            updates.append(create_value_update(rowcol_to_a1(cell.row, cell.col), [[value]]))

    return updates


def a1_range(from_row, from_col, to_row, to_col):
    """Utility to convert a 4 coordinate range to an A1 range."""
    return f'{rowcol_to_a1(from_row, from_col)}:{rowcol_to_a1(to_row, to_col)}'


def chunks(list, n, skip_rows=0):
    """Yield successive n-sized chunks from list."""
    for i in range(skip_rows * n, len(list), n):
        yield list[i:i + n]


@cast_to_a1_notation
def range_formula(worksheet, name):
    """Returns a list of :class:`Cell` objects from a specified range using valueRenderOption of
    'FORMULA'.
    :param name: A string with range value in A1 notation, e.g. 'A1:A5'.
    :type name: str
    Alternatively, you may specify numeric boundaries. All values
    index from 1 (one):
    :param int first_row: First row number
    :param int first_col: First column number
    :param int last_row: Last row number
    :param int last_col: Last column number
    """
    range_label = absolute_range_name(worksheet.title, name)

    data = worksheet.spreadsheet.values_get(range_label, params={'valueRenderOption': 'FORMULA'})

    start, end = name.split(':')
    (row_offset, column_offset) = a1_to_rowcol(start)
    (last_row, last_column) = a1_to_rowcol(end)

    values = data.get('values', [])

    rect_values = fill_gaps(
        values,
        rows=last_row - row_offset + 1,
        cols=last_column - column_offset + 1,
    )

    return [
        Cell(row=i + row_offset, col=j + column_offset, value=value)
        for i, row in enumerate(rect_values)
        for j, value in enumerate(row)
    ]
