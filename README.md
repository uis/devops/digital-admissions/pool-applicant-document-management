# Undergraduate Admissions Synchronisation Tool

This project contains tooling to perform synchronisation jobs required for the
Undergraduate Admissions process.

## Pool Google Drive Synchronisation

The `pooldrive update` command will synchronise a set of folders in a Google
Shared Drive with a list of applicants from the [Subject Moderation
Interface](https://gitlab.developers.cam.ac.uk/uis/devops/uga/smi/) using the
API.

Any subset of applications which can be returned by the API can be mapped,
one-to-one, into a set of "applicant folders" in a Google Shared Drive or
a sub-folder within a Google Shared Drive.

Each applicant folder will be named "{UCAS NUMBER} - {Applicant name}" and will
have both UCAS number, CamSIS id, college and subject information added to the
metadata so that folders can be searched for.

It is safe to run the command multiple times against the same query. Any new
folders which need to be created will be created and folders with the wrong name
will have their name updated.

**The command will not delete folders it does not recognise but will log a
warning when they are found.**

The command accesses the Google Shared Drive using a service account's identity and
so requires that a service account be created and added as a "Content Manager"
to the Shared Drive. [More information on
this](https://guidebook.devops.uis.cam.ac.uk/en/latest/notes/automating-google-drive/#creating-a-service-account)
is available in the DevOps guidebook.

## Installation

This product is packaged in two ways:

* A command line tool which can be used to test commands in development.
* A simple web application which can trigger commands by POST-ing to HTTP
    endpoints. It is intended that these endpoints are called regularly by some
    scheduled process.

To install locally via pip:

```bash
$ pip3 install --user git+https://gitlab.developers.cam.ac.uk/uis/devops/uga/pool-applicant-document-management.git
```

For local development you may find it easier to install an "editable" version in
a local virtualenv:

```bash
# At top-level of repository...
$ python3 -m virtualenv -p $(which python3) venv
$ . venv/bin/activate
$ pip install -e .
```

## Usage

In order to use this tool you will first need to have:

* Created a Google Shared Drive and, optionally, a folder within it for
    applicant folders to be created within.
* Obtained the alphanumeric id of the Shared Drive and, optionally, the id of
    the folder within it. Instructions for this are contained within the
    [example job specification file](jobspec.yml.example).
* [Created a Google service
    account](https://guidebook.devops.uis.cam.ac.uk/en/latest/notes/automating-google-drive/#creating-a-service-account)
* Created some JSON-formatted credentials for the service account and downloaded
    them.
* [Added that service account as a "content manager" to the Shared
    Drive](https://guidebook.devops.uis.cam.ac.uk/en/latest/notes/automating-google-drive/#sharing-resources-with-the-service-account)
* Obtained an API token for an instance of the Undergraduate Subject Moderation
    Interface API.

The SMI user associated with the token must have at least the following
permissions:

* `smi.view_application` (or equivalent allowing applications of interest to be
    viewed)
* `smi.change_externalresource`
* `smi.add_externalresource`

Make a copy of the [example configuration](jobspec.example.yml) called
`jobspec.yml` and provide the settings as documented in that file.

Configuration is specified via one or more URLs pointing at YAML-formatted job
specifications. The URL can have any of the schemes supported by the [geddit
library](https://gitlab.developers.cam.ac.uk/uis/devops/lib/geddit).

If multiple URLs are provided the settings are deep merged using the
[deepmerge](https://pypi.org/project/deepmerge/) package.

You can test your configuration via:

```bash
$ ugraddrivesync pooldrive update --dry-run $PWD/jobspec.yml
```

Similarly, assuming that the service account credentials are located in a file
named `credentials.json`, you can test the docker packaging via:

```bash
$ docker build -t ugraddrivesync . && docker run --rm \
    -e PORT=8000 -p 8000:8000 \
    -e GOOGLE_APPLICATION_CREDENTIALS=/credentials.json \
    -e JOBSPEC_URLS=/jobspec.yml \
    -v $PWD/credentials.json:/credentials.json:ro \
    -v $PWD/jobspec.yml:/jobspec.yml:ro \
    ugraddrivesync
```

(Note that `GOOGLE_APPLICATION_CREDENTIALS` is used to specify default
credentials and so the job specification file does not need to have
`credentials_path` set.)

Trigger the pool drive update via:

```bash
$ http POST http://localhost:8000/pooldrive/update
````

## Synchronising applications with CamSIS

The ugraddrivesync tool has a secondary (and unrelated) function which is to
synchronise a particular admission year of applications in SMI with the
matching applications in CamSIS. Only a subset of fields are synchronised and
only under certain conditions. The following list describes which fields are
synchronised and when:

  - differing `poolType` fields are always synchronised
  - differing `camsisStatus` fields are always synchronised
  - differing `subject` fields are synchronised when in the `SUMMER` or `ADJUSTMENT` pools
    and warned about otherwise
  - differing `admitYear` fields are synchronised when in the `SUMMER` or `ADJUSTMENT` pools
    and warned about otherwise
  - differing `collegePreference` fields are warned about unless the camSIS value is `POOL`

You can test your configuration via:

```bash
$ ugraddrivesync camsis update --dry-run $PWD/jobspec.yml
```

The docker packaging can be tested with the same docker commands as for testing
the pool drive update feature.

Trigger the update from CamSIS via:

```bash
$ http POST http://localhost:8000/camsis/update
````

It is noted here that this function has been added to this project for convenience and 
[isn't a very good fit as it's currently described](https://gitlab.developers.cam.ac.uk/uis/devops/uga/pool-applicant-document-management/-/issues/7).

## Synchronising applicants with EoIML

The ugraddrivesync tool has a secondary (and unrelated) function which is to synchronise a list of
applications queried from SMI with the "Expression of Interest" Google spreadsheet (EoIML). This
spreadsheet consists of a "CAO Master List" worksheet with all of the pool
applications and a worksheet for each of the colleges. The first 7 columns are sync targets and
other columns are ignored. Rows are matched using the application id and are add and amended as
appropriate but not deleted.

You can test your configuration via:

```bash
$ ugraddrivesync eoiml update --dry-run $PWD/jobspec.yml
```

The docker packaging can be tested with the same docker commands as for testing
the pool drive update feature.

Trigger the update from CamSIS via:

```bash
$ http POST http://localhost:8000/eoiml/update
````

It is noted here that this function has been added to this project for convenience and 
[isn't a very good fit as it's currently described](https://gitlab.developers.cam.ac.uk/uis/devops/uga/pool-applicant-document-management/-/issues/7).

## Qualtrics Synchronisation

Another function exposed by the ugraddrivesync tool is the qualtrics integration. This allows Qualtrics survey responses
to be automatically pdf-ed and uploaded into the appropriate applicant's google drive folder.

Qualtrics can be accessed via our Cambridge Qualtrics instance: https://cambridge.eu.qualtrics.com/.
Access to the Qualtrics API is disabled by default. It can be enabled for specific accounts by raising a service desk 
ticket. Account details for our service account are available in 1Password.

### Testing Qualtrics Sync:
To make testing easier, a partial configuration file is available in [1Password](https://uis-devops.1password.eu/vaults/eop52ita2oqwjcjfjawcmzj5za/allitems/5vlguxvp4d6fw632tuwx7epeke).
- Download the configuration file
- Complete the `google_drive` part of the config (this will be similar to the `pooldrive` tool config).
- Create the Google folder for mismatches and set it's id in the config.
- Run the tool: `$ ugraddrivesync qualtrics sync $PWD/qualtrics.yml`

You will need to amend the questionnaire while you are testing. This can be done by browsing to
https://cambridge.eu.qualtrics.com and signing in with the
[service account credentials in 1password](https://uis-devops.1password.eu/vaults/eop52ita2oqwjcjfjawcmzj5za/allitems/wyeq2i2o5zcadlpq7jhznk42oa
)

## CAPO Synchronisation

Yet another function exposed by the ugraddrivesync tool is the CAPO synchronisation. This allows
CAPOs (and transcripts) to be automatically fetched from CamSIS as PDFs and uploaded into the
appropriate applicant's google drive folder.

You can test your configuration via:

```bash
$ ugraddrivesync capo sync --dry-run $PWD/jobspec.yml
```

The docker packaging can be tested with the same docker commands as for testing
the pool drive update feature.

Trigger the sync via:

```bash
$ http POST http://localhost:8000/capo/sync
```

## Updating pip requirements

Install pip-tools in your virtualenv, maybe with `venv/bin/pip3 install pip-tools`.

Edit requirements.in as you would requirements.txt including -e, -r and -c options as required.

Remove the existing txt files and compile the requirements.txt files with `cd requirements && rm *.txt && for file in requirements*.in; do pip-compile $file; done`. This will write out new requirements files with version-pinned packages that are compatible with all dependencies of all requirements, even those in referenced requirements files.

Old dependencies are cleaned up so it is important to run `pip-sync requirements-dev.txt` rather than just pip install so that old package versions are removed and existing packages are upgraded to the correct version.

